<?php

namespace App\Http\Controllers;

use App\Models\HiaringMood;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AdminHiaringMoodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hmoods = HiaringMood::all();
        return view('admin.hiaring_mood.index', compact('hmoods'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.hiaring_mood.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->request->add(['slug' => Str::slug($request->name)]);
        $request->validate(
            [
                'name'  => 'required|string',
                'slug'  => 'unique:hiaring_moods,slug'
            ],
            [
                'name.required' => 'Hiaring Mood is Required.',
                'name.string'   => 'Provide Valid Hiaring Mood.',
                'slug.unique'   => 'Hiaring Mood Name Already Exist.',
            ]
        );

        $newHiaringMood = new HiaringMood();
        $newHiaringMood->name = $request->name;
        $newHiaringMood->slug = Str::slug($request->name);
        $newHiaringMood->save();

        session()->flash('success', 'Hiaring Mood Created Successfully.');
        return redirect()->route('hiaring-moods.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HiaringMood  $hiaringMood
     * @return \Illuminate\Http\Response
     */
    public function show(HiaringMood $hiaringMood)
    {
        // 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HiaringMood  $hiaringMood
     * @return \Illuminate\Http\Response
     */
    public function edit(HiaringMood $hiaringMood)
    {
        return view('admin.hiaring_mood.create', compact('hiaringMood'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HiaringMood  $hiaringMood
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HiaringMood $hiaringMood)
    {
        $request->request->add(['slug' => Str::slug($request->name)]);
        $request->validate(
            [
                'name'  => 'required|string',
                'slug'  => 'unique:hiaring_moods,slug,' . $hiaringMood->id
            ],
            [
                'name.required' => 'Hiaring Mood is Required.',
                'name.string'   => 'Provide Valid Hiaring Mood.',
                'slug.unique'   => 'Hiaring Mood Name Already Exist.',
            ]
        );

        $hiaringMood->name = $request->name;
        $hiaringMood->slug = Str::slug($request->name);
        $hiaringMood->save();

        session()->flash('success', 'Hiaring Mood Created Successfully.');
        return redirect()->route('hiaring-moods.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HiaringMood  $hiaringMood
     * @return \Illuminate\Http\Response
     */
    public function destroy(HiaringMood $hiaringMood)
    {
        //
    }
}
