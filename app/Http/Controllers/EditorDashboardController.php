<?php

namespace App\Http\Controllers;

use App\Models\EditorClient;
use Carbon\Carbon;
use Illuminate\Http\Request;

class EditorDashboardController extends Controller
{
    public function index()
    {
        $fromdate = Carbon::now()->toDateString();
        $todate = Carbon::now()->addDays(60)->toDateString();

        $allClient = EditorClient::where('company_id', auth()->user()->company_id)->get()->count();
        $nearToPassportExpireClient = EditorClient::where('company_id', auth()->user()->company_id)->whereBetween('passport_expire_date', [$fromdate, $todate])->get()->count();
        $passportExpiredClient = EditorClient::where('company_id', auth()->user()->company_id)->whereDate('passport_expire_date', '<', $fromdate)->get()->count();
        return view('editor.dashboard.index', compact('allClient', 'nearToPassportExpireClient', 'passportExpiredClient'));
    }
}
