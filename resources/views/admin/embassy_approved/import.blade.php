@extends('layouts.layout')

@section('title', 'Embassy User Import')

@section('content')

@component('partials.breadcrumb',[
'title' => 'Import Embassy Approved User',
'activePage' => 'Embassy User Import'
])
@endcomponent
<section class="content">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Import Users</h3>
        </div>
        <form action="{{ route('embassy-approve-import') }}" enctype="multipart/form-data" method="POST">
            @csrf
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group @error('user_list_file') has-error @enderror">
                            <label for="userCsv" class="control-label">Upload User list (csv) <span class="text-danger">*</span></label>
                            <input type="file" name="user_list_file" class="form-control" id="userCsv">
                            @error('user_list_file')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <a href="{{ route('embassy-approve.index') }}" class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-info pull-right">Upload</button>
            </div>
        </form>
    </div>
</section>
@endsection