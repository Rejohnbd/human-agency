@extends('layouts.layout')

@if(isset($hiaringMood))
@section('title', 'Hiaring Mood Update')
@else
@section('title', 'Hiaring Mood Create')
@endif

@section('content')

@component('partials.breadcrumb',[
'title' => isset($hiaringMood) ? 'Hiaring Mood Update' : 'Hiaring Mood Create',
'activePage' => isset($hiaringMood) ? 'Hiaring Mood Update' : 'Hiaring Mood Create'
])
@endcomponent

<section class="content">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">@if(isset($hiaringMood)) Update Hiaring Mood @else Create Hiaring Mood @endif</h3>
        </div>
        <form action="{{ isset($hiaringMood) ? route('hiaring-moods.update', $hiaringMood->id) : route('hiaring-moods.store') }}" method="POST">
            @csrf
            @if(isset($hiaringMood))
            @method('PUT')
            @endif
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group @error('name') has-error @enderror">
                            <label for="firstName" class="control-label">Hiaring Mood Name <span class="text-danger">*</span></label>
                            <input type="text" name="name" class="form-control" id="hiaringMood" value="{{ isset($hiaringMood) ? $hiaringMood->name : old('name') }}" placeholder="Enter Hiaring Mood Name" required>
                            @error('name')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                            @error('slug')
                            <span class="help-block text-danger" style="color: red !important;">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <a href="{{ route('hiaring-moods.index') }}" class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-info pull-right">@if(isset($hiaringMood)) Update Hiaring Mood @else Create Hiaring Mood @endif</button>
            </div>
        </form>
    </div>
</section>
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('vendors/sweet-alert/sweetalert.css') }}">
@endsection

@section('scripts')
<script src="{{ asset('vendors/sweet-alert/sweetalert.js') }}"></script>

@if(session('error'))
<script>
    $(document).ready(function() {
        Swal.fire({
            title: "Alert",
            text: "{{ session('error') }}",
            icon: "error",
            showCancelButton: true,
            confirmButtonText: 'Exit',
            cancelButtonText: 'Stay on the page'
        });
    });
</script>
@endif
@endsection