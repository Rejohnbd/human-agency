@extends('layouts.layout')

@section('title', 'Users List')

@section('content')

@component('partials.breadcrumb',[
'title' => 'User List',
'activePage' => 'User List'
])
@endcomponent

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <i class="fa fa-user"></i>
                    <h3 class="box-title">User List</h3>
                    <div class="box-tools">
                        <a href="{{ route('users.create') }}" type="button" class="btn btn-info btn-flat btn-xs pull-right">Add User</a>
                    </div>
                </div>
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th>#</th>
                                <th>User Name</th>
                                <th>Company Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Create Date</th>
                                <th>User Type</th>
                                <th>Action</th>
                            </tr>
                            @forelse ($datas as $data)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $data->first_name }} {{ $data->last_name }}</td>
                                <td>@if(isset($data->company->company_name)){{ $data->company->company_name }}@endif</td>
                                <td>{{ $data->email }}</td>
                                <td>{{ $data->phone }}</td>
                                <td>{{ date('d-m-Y',strtotime($data->created_at)) }}</td>
                                <td><span class="label @if($data->role->slug === 'admin') label-success @elseif ($data->role->slug === 'editor') label-info @else label-warning @endif">{{ $data->role->name }}</span></td>
                                <td>
                                    {{-- <a href="#" type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Show {{ $data->first_name }} {{ $data->last_name }} Details"><i class="fa fa-eye"></i></a> --}}
                                    @if($data->editable)
                                    <a href="{{ route('users.edit', $data->id) }}" type="button" class="btn btn-sm btn-info" data-toggle="tooltip" data-placement="top" title="Edit {{ $data->first_name }} {{ $data->last_name }} info"><i class="fa fa-edit"></i></a>
                                    @endif
                                    @if($data->deleteable)
                                    <a href="#" type="button" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="Delete {{ $data->first_name }} {{ $data->last_name }}"><i class="fa fa-trash"></i></a>
                                    @endif
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="6" class="text-center">No User Created.</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('vendors/sweet-alert/sweetalert.css') }}">
@endsection

@section('scripts')
<script src="{{ asset('vendors/sweet-alert/sweetalert.js') }}"></script>
@if(session('success'))
<script>
    $(document).ready(function() {
        Swal.fire(
            'Good job!',
            "{{ session('success') }}",
            'success'
        );
    })
</script>
@endif

@if(session('error'))
<script>
    $(document).ready(function() {
        Swal.fire({
            title: "Alert",
            text: "{{ session('error') }}",
            icon: "error",
            showCancelButton: true,
            confirmButtonText: 'Exit',
            cancelButtonText: 'Stay on the page'
        });
    });
</script>
@endif
@endsection