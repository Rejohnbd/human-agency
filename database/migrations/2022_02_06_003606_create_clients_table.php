<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->comment('FK from table:user');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('passport_no', 30)->unique();
            $table->string('bmet_reg_no', 30)->nullable();
            $table->string('nid', 20)->unique();
            $table->date('date_of_birth');
            $table->date('passport_issue_date');
            $table->date('passport_expire_date');
            $table->string('mobile', 20)->unique();
            $table->string('mobile_optional', 20)->nullable();
            $table->string('home_mobile');
            $table->string('country_id', 11);
            $table->decimal('net_amount', 10, 2);
            $table->decimal('discount_amount', 10, 2)->nullable();
            $table->decimal('paid_amount', 10, 2);
            $table->decimal('total_amount', 10, 2)->nullable();
            $table->text('address');
            $table->text('comments')->nullable();
            $table->tinyInteger('edit_status')->default(0);
            $table->tinyInteger('delete_status')->default(0);
            $table->tinyInteger('completed_status')->default(0);
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
