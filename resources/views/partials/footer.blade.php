<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Developed by</b> <a href="https://github.com/rejohnbd" target="_blank">Rejohn</a>
    </div>
    <strong>Copyright &copy; 2021-{{ date('Y') }}. All rightsreserved.</strong>
</footer>