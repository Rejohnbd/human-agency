<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmbassyApprove\ApprovedClientStore;
use App\Http\Requests\EmbassyApprove\ApprovedClientUpdate;
use App\Imports\AmbassyApproveImport;
use App\Models\Country;
use App\Models\EmbassyApprove;
use Exception;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Validator;

class AdminEmbassyApproveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $datas = EmbassyApprove::orderBy('id', 'desc')->paginate(10);
        // return view('embassy_approved.index', compact('datas'));
        return view('admin.embassy_approved.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $allCountries = Country::where('continent_name', 'Asia')->get(['id', 'country_name']);
        return view('admin.embassy_approved.create', compact('allCountries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ApprovedClientStore $request)
    {
        $passport_expire_date = null;
        if (!is_null($request->passport_expire_date)) {
            $passport_expire_date = date('Y-m-d', strtotime(str_replace('/', '-', $request->passport_expire_date)));
        }

        $newClient = new EmbassyApprove;

        $newClient->passport_no             = $request->passport_no;
        $newClient->name                    = $request->name;
        $newClient->country_id              = $request->country_id;
        $newClient->mobile                  = $request->mobile;
        $newClient->passport_expire_date    = $passport_expire_date;
        $newClient->registration_date       = date('Y-m-d', strtotime(str_replace('/', '-', $request->registration_date)));

        $saveNewClient = $newClient->save();

        if ($saveNewClient) {
            session()->flash('success', 'Client Created Successfully.');
            return redirect()->route('embassy-approve.index');
        } else {
            session()->flash('error', 'Something Happend Wrong');
            return redirect()->route('embassy-approve.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EmbassyApprove  $embassyApprove
     * @return \Illuminate\Http\Response
     */
    public function show(EmbassyApprove $embassyApprove)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EmbassyApprove  $embassyApprove
     * @return \Illuminate\Http\Response
     */
    public function edit(EmbassyApprove $embassyApprove)
    {
        $allCountries = Country::where('continent_name', 'Asia')->get(['id', 'country_name']);
        return view('admin.embassy_approved.create', compact('embassyApprove', 'allCountries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EmbassyApprove  $embassyApprove
     * @return \Illuminate\Http\Response
     */
    public function update(ApprovedClientUpdate $request, EmbassyApprove $embassyApprove)
    {
        $checkPassport = EmbassyApprove::where('passport_no', $request->passport_no)->whereNotIn('id', [$embassyApprove->id])->get();
        if (count($checkPassport) > 0) {
            session()->flash('error', 'This Passport Number already used.');
            return redirect()->route('embassy-approve.index');
        } else {
            $passport_expire_date = null;
            if (!is_null($request->passport_expire_date)) {
                $passport_expire_date = date('Y-m-d', strtotime(str_replace('/', '-', $request->passport_expire_date)));
            }

            $updateClient = $embassyApprove->update([
                'passport_no'           => $request->passport_no,
                'name'                  => $request->name,
                'country_id'            => $request->country_id,
                'mobile'                => $request->mobile,
                'passport_expire_date'  => $passport_expire_date,
                'registration_date'     => date('Y-m-d', strtotime(str_replace('/', '-', $request->registration_date))),
            ]);

            if ($updateClient) {
                session()->flash('success', 'Client Updated Successfully.');
                return redirect()->route('embassy-approve.index');
            } else {
                session()->flash('error', 'Something Happend Wrong');
                return redirect()->route('embassy-approve.index');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EmbassyApprove  $embassyApprove
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmbassyApprove $embassyApprove)
    {
        //
    }

    public function embassyApproveList(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'passport_no',
            2 => 'name',
            3 => 'country_name',
            4 => 'mobile',
            5 => 'passport_expire_date',
            6 => 'registration_date',
        );

        $totalData = EmbassyApprove::count();
        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $clients = EmbassyApprove::with('country')->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $clients =  EmbassyApprove::with('country')->where('id', 'LIKE', "%{$search}%")
                ->orWhere('name', 'LIKE', "%{$search}%")
                ->orWhere('passport_no', 'LIKE', "%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = EmbassyApprove::with('country')->where('id', 'LIKE', "%{$search}%")
                ->orWhere('name', 'LIKE', "%{$search}%")
                ->orWhere('passport_no', 'LIKE', "%{$search}%")
                ->count();
        }

        $data = array();
        if (!empty($clients)) {
            foreach ($clients as $client) {
                // $show =  route('posts.show', $client->id);
                // $edit =   route('embassy-approve.edit', $client->id);

                $nestedData['id'] = $client->id;
                $nestedData['passport_no'] = $client->passport_no;
                $nestedData['name'] = $client->name;
                $nestedData['country_name'] = $client->country->country_name;
                $nestedData['mobile'] = $client->mobile;
                $nestedData['passport_expire_date'] = $client->passport_expire_date ? date('d/m/Y', strtotime($client->passport_expire_date)) : '';
                $nestedData['registration_date'] = date('d/m/Y', strtotime($client->registration_date));
                $nestedData['action'] = '<a href=' . route('embassy-approve.edit', $client->id) . ' type="button" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Edit ' . $client->name . ' info"><i class="fa fa-edit"></i></a>';
                // $nestedData['body'] = substr(strip_tags($client->body), 0, 50) . "...";
                // $nestedData['options'] = "&emsp;<a href='{$show}' title='SHOW' ><span class='glyphicon glyphicon-list'></span></a> &emsp;<a href='{$edit}' title='EDIT' ><span class='glyphicon glyphicon-edit'></span></a>";
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    public function embassyApproveImport()
    {
        return view('admin.embassy_approved.import');
    }

    public function embassyApproveSave(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make(
            $request->all(),
            [
                'user_list_file' => 'required|file|mimes:csv,txt'
            ],
            [
                'user_list_file.required'   => 'User List File Required.',
                'user_list_file.mimes'      => 'User List File Must be csv format.'
            ]
        );
        $validator->validate();

        try {
            Excel::import(new AmbassyApproveImport, $request->user_list_file);

            session()->flash('success', 'Client Uploaded Successfully.');
            return redirect()->route('embassy-approve.index');
        } catch (Exception $e) {
            session()->flash('error', 'File format not correct or Dublicate entity. Please Try Again');
            return redirect()->route('embassy-approve.index');
        }
    }
}
