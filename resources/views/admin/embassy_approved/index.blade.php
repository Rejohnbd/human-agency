@extends('layouts.layout')

@section('title', 'Embassy Approved List')

@section('content')

@component('partials.breadcrumb',[
'title' => 'Embassy Approved',
'activePage' => 'Embassy Approved List'
])
@endcomponent

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <div class="box-header">
                        <i class="fa fa-users"></i>
                        <h3 class="box-title">Embassy Approved Client List</h3>
                        <div class="box-tools">
                            <a href="{{ route('embassy-approve.create') }}" type="button" class="btn btn-info btn-flat btn-xs pull-right">Add Client</a>
                        </div>
                    </div>
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Passport No</th>
                                    <th>Name</th>
                                    <th>Country</th>
                                    <th>Mobile</th>
                                    <th>Passport Expire Date</th>
                                    <th>Registration Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
</section>
@endsection


@section('styles')
<link rel="stylesheet" href="{{ asset('vendors/sweet-alert/sweetalert.css') }}">
<link rel="stylesheet" href="{{ asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('scripts')
<script src="{{ asset('vendors/sweet-alert/sweetalert.js') }}"></script>
<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script>
    $(document).ready(function() {
        var table = $('#example2').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                "url": "{{ route('embassy-approve-list') }}",
                "dataType": "json",
                "type": "POST",
                "data": {
                    _token: "{{csrf_token()}}"
                }
            },
            columns: [{
                    "data": "id"
                },
                {
                    "data": "passport_no"
                },
                {
                    "data": "name"
                },
                {
                    "data": "country_name"
                },
                {
                    "data": "mobile"
                },
                {
                    "data": "passport_expire_date"
                },
                {
                    "data": "registration_date"
                },
                {
                    "data": "action"
                },
            ]
        });
    });
</script>
@if(session('success'))
<script>
    $(document).ready(function() {
        Swal.fire(
            'Good job!',
            "{{ session('success') }}",
            'success'
        );
    });
</script>
@endif

@if(session('error'))
<script>
    $(document).ready(function() {
        Swal.fire({
            title: "Alert",
            text: "{{ session('error') }}",
            icon: "error",
            showCancelButton: true,
            confirmButtonText: 'Exit',
            cancelButtonText: 'Stay on the page'
        });
    });
</script>
@endif
@endsection