<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmbassyApprovesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('embassy_approves', function (Blueprint $table) {
            $table->id();
            $table->string('passport_no', 30)->unique();
            $table->string('name', 30);
            $table->string('country_id', 11);
            $table->string('mobile', 20)->nullable();
            $table->date('passport_expire_date')->nullable();
            $table->date('registration_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('embassy_approves');
    }
}
