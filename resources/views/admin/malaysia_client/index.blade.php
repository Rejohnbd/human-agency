@extends('layouts.layout')

@section('title', 'Malaysia Client List')

@section('content')

@component('partials.breadcrumb',[
'title' => 'Malaysia Client List',
'activePage' => 'Malaysia Client List'
])
@endcomponent

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <i class="fa fa-users"></i>
                    <h3 class="box-title">Malaysia Client List</h3>
                    <div class="box-tools">
                        <a href="{{ route('malaysia-clients.create') }}" type="button" class="btn btn-info btn-flat btn-xs pull-right">Add Client</a>
                    </div>
                    <div class="box-tools">
                        <div class="input-group input-group-sm hidden-xs" style="width: 150px; margin-right: 10px">
                            <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="box-tools m">
                        <div class="input-group input-group-sm hidden-xs" style="width: 150px; margin-right: 10px">
                            <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                        </div>
                    </div>
                    <div class="box-tools">
                        <div class="input-group input-group-sm hidden-xs" style="width: 150px; margin-right: 10px">
                            <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                        </div>
                    </div>
                </div>
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Country</th>
                                <th>Passport No</th>
                                <th>Phone</th>
                                <th>Net Amount</th>
                                <th>Discount Amount</th>
                                <th>Paid Amount</th>
                                <th>Total Pay</th>
                                <th>Created Date</th>
                                <th>Created By</th>
                                <th>Action</th>
                            </tr>
                            @forelse ($datas as $data)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $data->first_name }} {{ $data->last_name }}</td>
                                <td>{{ $data->country->country_name }}</td>
                                <td>{{ $data->passport_no }}</td>
                                <td>{{ $data->mobile }}</td>
                                <td>{{ $data->net_amount }}</td>
                                <td>{{ $data->discount_amount }}</td>
                                <td>{{ $data->paid_amount }}</td>
                                <td>{{ $data->total_amount }}</td>
                                <td>{{ date('d/m/Y', strtotime($data->created_at)) }}</td>
                                <td><span class="label @if($data->user->role->slug === 'admin') label-success @elseif ($data->user->role->slug === 'editor') label-info @else label-warning @endif">{{ $data->user->first_name }} {{ $data->user->last_name }}</span></td>
                                <td>
                                    <a href="{{ route('malaysia-clients.show', $data->id) }}" type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Show {{ $data->first_name }} {{ $data->last_name }} Details"><i class="fa fa-eye"></i></a>
                                    <a href="{{ route('malaysia-clients.edit', $data->id) }}" type="button" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Edit {{ $data->first_name }} {{ $data->last_name }} info"><i class="fa fa-edit"></i></a>
                                    <a href="#" type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete {{ $data->first_name }} {{ $data->last_name }}"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="12" class="text-center">No Client Created.</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                    <div class="text-center">
                        <ul class="pagination pagination-sm no-margin">
                            {!! $datas->render() !!}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('vendors/sweet-alert/sweetalert.css') }}">
@endsection

@section('scripts')
<script src="{{ asset('vendors/sweet-alert/sweetalert.js') }}"></script>
@if(session('success'))
<script>
    $(document).ready(function() {
        Swal.fire(
            'Good job!',
            "{{ session('success') }}",
            'success'
        );
    })
</script>
@endif

@if(session('error'))
<script>
    $(document).ready(function() {
        Swal.fire({
            title: "Alert",
            text: "{{ session('error') }}",
            icon: "error",
            showCancelButton: true,
            confirmButtonText: 'Exit',
            cancelButtonText: 'Stay on the page'
        });
    });
</script>
@endif
@endsection