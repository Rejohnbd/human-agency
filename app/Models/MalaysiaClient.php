<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class MalaysiaClient extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }

    public function medicalStatus()
    {
        return $this->belongsTo(MedicalStatuss::class, 'medical_status', 'id')->withDefault();
    }
}
