@extends('layouts.layout')

@if(isset($medicalStatus))
@section('title', 'Medical Status Update')
@else
@section('title', 'Medical Status Create')
@endif

@section('content')

@component('partials.breadcrumb',[
'title' => isset($medicalStatus) ? 'Medical Status Update' : 'Medical Status Create',
'activePage' => isset($medicalStatus) ? 'Medical Status Update' : 'Medical Status Create'
])
@endcomponent

<section class="content">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">@if(isset($medicalStatus)) Update Medical Status @else Create Medical Status @endif</h3>
        </div>
        <form action="{{ isset($medicalStatus) ? route('medical-status.update', $medicalStatus->id) : route('medical-status.store') }}" method="POST">
            @csrf
            @if(isset($medicalStatus))
            @method('PUT')
            @endif
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group @error('name') has-error @enderror">
                            <label for="firstName" class="control-label">Medical Status Name <span class="text-danger">*</span></label>
                            <input type="text" name="name" class="form-control" id="firstName" value="{{ isset($medicalStatus) ? $medicalStatus->name : old('name') }}" placeholder="Enter Medical Status Name" required>
                            @error('name')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <a href="{{ route('medical-status.index') }}" class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-info pull-right">@if(isset($medicalStatus)) Update Medical Status @else Create Medical Status @endif</button>
            </div>
        </form>
    </div>
</section>
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('vendors/sweet-alert/sweetalert.css') }}">
@endsection

@section('scripts')
<script src="{{ asset('vendors/sweet-alert/sweetalert.js') }}"></script>

@if(session('error'))
<script>
    $(document).ready(function() {
        Swal.fire({
            title: "Alert",
            text: "{{ session('error') }}",
            icon: "error",
            showCancelButton: true,
            confirmButtonText: 'Exit',
            cancelButtonText: 'Stay on the page'
        });
    });
</script>
@endif
@endsection