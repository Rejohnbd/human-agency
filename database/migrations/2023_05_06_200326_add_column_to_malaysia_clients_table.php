<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToMalaysiaClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('malaysia_clients', function (Blueprint $table) {
            $table->string('work_id')->after('last_name')->nullable();
            $table->tinyInteger('collected_from')->after('medical_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('malaysia_clients', function (Blueprint $table) {
            $table->dropColumn('work_id');
            $table->dropColumn('collected_from');
        });
    }
}
