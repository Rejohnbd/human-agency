@extends('layouts.layout')

@section('title', 'Company List')

@section('content')

@component('partials.breadcrumb',[
'title' => 'Company List',
'activePage' => 'Company List'
])
@endcomponent

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <i class="fa fa-users"></i>
                    <h3 class="box-title">Company List</h3>
                    <div class="box-tools">
                        <a href="{{ route('companies.create') }}" type="button" class="btn btn-info btn-flat btn-xs pull-right">Add Company</a>
                    </div>
                </div>
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th>#</th>
                                <th>Company Name</th>
                                <th>Number of Client</th>
                                <th>Number of Editor</th>
                                <th>Action</th>
                            </tr>
                            @forelse ($datas as $data)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $data->company_name }}</td>
                                <td>{{ $data->clients->count() }}</td>
                                <td>{{ $data->editors->count() }}</td>
                                <td>
                                    <a href="{{ route('companies.edit', $data->id) }}" type="button" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Edit {{ $data->company_name }} info"><i class="fa fa-edit"></i></a>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="12" class="text-center">No Company Created.</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('vendors/sweet-alert/sweetalert.css') }}">
@endsection

@section('scripts')
<script src="{{ asset('vendors/sweet-alert/sweetalert.js') }}"></script>
@if(session('success'))
<script>
    $(document).ready(function() {
        Swal.fire(
            'Good job!',
            "{{ session('success') }}",
            'success'
        );
    })
</script>
@endif

@if(session('error'))
<script>
    $(document).ready(function() {
        Swal.fire({
            title: "Alert",
            text: "{{ session('error') }}",
            icon: "error",
            showCancelButton: true,
            confirmButtonText: 'Exit',
            cancelButtonText: 'Stay on the page'
        });
    });
</script>
@endif
@endsection