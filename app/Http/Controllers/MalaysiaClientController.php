<?php

namespace App\Http\Controllers;

use App\Http\Requests\MalaysiaClient\MalaysiaClientStoreRequest;
use App\Http\Requests\MalaysiaClient\MalaysiaClientUpdateRequest;
use App\Models\Country;
use App\Models\MalaysiaClient;
use App\Models\MedicalStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MalaysiaClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = MalaysiaClient::orderBy('id', 'desc')->paginate(10);
        return view('admin.malaysia_client.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $allCountries = Country::where('continent_name', 'Asia')->get(['id', 'country_name']);
        $allMedicalStatus = MedicalStatus::all();
        return view('admin.malaysia_client.create', compact('allCountries', 'allMedicalStatus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MalaysiaClientStoreRequest $request)
    {
        // dd('here');
        $checkMedicalStatus = MedicalStatus::find($request->medical_status);
        if (is_null($checkMedicalStatus)) {
            session()->flash('error', 'Please Provide Valid Medical Status');
            return redirect()->route('malaysia-clients.index');
        } else {
            $newClient = new MalaysiaClient;

            $newClient->user_id                 = Auth::user()->id;
            $newClient->first_name              = $request->first_name;
            $newClient->last_name               = $request->last_name;
            $newClient->date_of_birth           = date('Y-m-d', strtotime(str_replace('/', '-', $request->date_of_birth)));
            $newClient->passport_no             = $request->passport_no;
            $newClient->passport_issue_date     = date('Y-m-d', strtotime(str_replace('/', '-', $request->passport_issue_date)));
            $newClient->passport_expire_date    = date('Y-m-d', strtotime(str_replace('/', '-', $request->passport_expire_date)));
            $newClient->mobile                  = $request->mobile;
            $newClient->home_mobile             = $request->home_mobile;
            $newClient->country_id              = $request->country_id;
            $newClient->net_amount              = $request->net_amount;
            $newClient->discount_amount         = $request->discount_amount;
            $newClient->paid_amount             = $request->net_amount - $request->discount_amount;
            $newClient->address                 = $request->address;
            $newClient->comments                = $request->comments;
            $newClient->medical_status          = $request->medical_status;
            $newClient->work_id                 = $request->work_id;
            $newClient->collected_from          = $request->collected_from;

            $saveNewClient = $newClient->save();

            if ($saveNewClient) {
                session()->flash('success', 'Client Created Successfully.');
                return redirect()->route('malaysia-clients.index');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(MalaysiaClient $malaysiaClient)
    {
        $allCountries = Country::where('continent_name', 'Asia')->get(['id', 'country_name']);
        $allMedicalStatus = MedicalStatus::all();
        return view('admin.malaysia_client.create', compact('allCountries', 'allMedicalStatus'))->with(['client' => $malaysiaClient]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MalaysiaClientUpdateRequest $request, MalaysiaClient $malaysiaClient)
    {
        $checkMedicalStatus = MedicalStatus::find($request->medical_status);
        if (is_null($checkMedicalStatus)) {
            session()->flash('error', 'Please Provide Valid Medical Status');
            return redirect()->route('malaysia-clients.index');
        } else {
            $updateClient = $malaysiaClient->update([
                'first_name'              => $request->first_name,
                'last_name'               => $request->last_name,
                'date_of_birth'           => date('Y-m-d', strtotime(str_replace('/', '-', $request->date_of_birth))),
                'passport_no'             => $request->passport_no,
                'passport_issue_date'     => date('Y-m-d', strtotime(str_replace('/', '-', $request->passport_issue_date))),
                'passport_expire_date'    => date('Y-m-d', strtotime(str_replace('/', '-', $request->passport_expire_date))),
                'mobile'                  => $request->mobile,
                'home_mobile'             => $request->home_mobile,
                'country_id'              => $request->country_id,
                'net_amount'              => $request->net_amount,
                'medical_status'          => $request->medical_status,
                'medical_status'          => $request->medical_status,
                'discount_amount'         => $request->discount_amount,
                'paid_amount'             => $request->net_amount - $request->discount_amount,
                'address'                 => $request->address,
                'comments'                => $request->comments,
                'work_id'                 => $request->work_id,
                'collected_from'          => $request->collected_from
            ]);

            if ($updateClient) {
                session()->flash('success', 'Client Updated Successfully.');
                return redirect()->route('malaysia-clients.index');
            } else {
                session()->flash('error', 'Something Happend Wrong');
                return redirect()->route('malaysia-clients.index');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
