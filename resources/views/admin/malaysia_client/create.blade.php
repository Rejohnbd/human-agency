@extends('layouts.layout')

@if(isset($client))
@section('title', 'Malaysia Client Update')
@else
@section('title', 'Malaysia Client Create')
@endif

@section('content')

@component('partials.breadcrumb',[
'title' => isset($client) ? 'Malaysia Client Update' : 'Malaysia Client Create',
'activePage' => isset($client) ? 'Malaysia Client Update' : 'Malaysia Client Create'
])
@endcomponent

<section class="content">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">@if(isset($client)) Malaysia Client Update @else Malaysia Client Create @endif</h3>
        </div>
        <form action="{{ isset($client) ? route('malaysia-clients.update', $client->id) : route('malaysia-clients.store') }}" method="POST">
            @csrf
            @if(isset($client))
            @method('PUT')
            @endif
            <div class="box-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group @error('first_name') has-error @enderror">
                            <label for="firstName" class="control-label">Client First Name <span class="text-danger">*</span></label>
                            <input type="text" name="first_name" class="form-control" id="firstName" value="{{ isset($client) ? $client->first_name : old('first_name') }}" placeholder="Enter First Name" required>
                            @error('first_name')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group @error('last_name') has-error @enderror">
                            <label for="lastName" class="control-label">Client Last Name <span class="text-danger">*</span></label>
                            <input type="text" name="last_name" class="form-control" id="lastName" value="{{ isset($client) ? $client->last_name : old('last_name') }}" placeholder="Enter Last Name" required>
                            @error('last_name')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group @error('work_id') has-error @enderror">
                            <label for="workId" class="control-label">Client Work Id <span class="text-danger">*</span></label>
                            <input type="text" name="work_id" class="form-control" id="workId" value="{{ isset($client) ? $client->work_id : old('work_id') }}" placeholder="Enter Work Id">
                            @error('work_id')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group @error('passport_no') has-error @enderror">
                            <label for="passportNo" class="control-label">Passport Number <span class="text-danger">*</span></label>
                            <input type="text" name="passport_no" class="form-control" id="passportNo" value="{{ isset($client) ? $client->passport_no : old('passport_no') }}" placeholder="Enter Passport No" required>
                            @error('passport_no')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group @error('mobile') has-error @enderror">
                            <label for="phoneNumber" class="control-label">Phone Number <span class="text-danger">*</span></label>
                            {{-- <input type="text" name="mobile" class="form-control" id="phoneNumber" value="{{ isset($client) ? $client->mobile : old('mobile') }}" data-inputmask="&quot;mask&quot;: &quot;99999999999&quot;" data-mask="" placeholder="Phone Number" required> --}}
                            <input type="text" name="mobile" class="form-control" id="phoneNumber" value="{{ isset($client) ? $client->mobile : old('mobile') }}" placeholder="Phone Number" required>
                            @error('mobile')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group @error('home_mobile') has-error @enderror">
                            <label for="homePhoneNumber" class="control-label">Home Phone Number</label>
                            {{-- <input type="text" name="home_mobile" class="form-control" id="homePhoneNumber" value="{{ isset($client) ? $client->home_mobile : old('home_mobile') }}" data-inputmask="&quot;mask&quot;: &quot;99999999999&quot;" data-mask="" placeholder="Home Phone Number" required> --}}
                            <input type="text" name="home_mobile" class="form-control" id="homePhoneNumber" value="{{ isset($client) ? $client->home_mobile : old('home_mobile') }}" placeholder="Home Phone Number">
                            @error('home_mobile')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label for="selectMedicalStatus" class="control-label">Select Medical Status <span class="text-danger">*</span></label>
                        <select class="form-control" name="medical_status" id="selectMedicalStatus" style="width: 100%;" required>
                            @if(!isset($client) || is_null($client->medical_status))
                            <option>Select Medical Status</option>
                            @endif
                            @foreach($allMedicalStatus as $medicalStatus)
                            <option value="{{ $medicalStatus->id }}" @if(isset($client)) @if($medicalStatus->id == $client->medical_status) selected @endif @endif>{{ $medicalStatus->name }}</option>
                            @endforeach
                        </select>
                        @error('medical_status')
                        <span class="help-block text-danger" style="color: red !important;">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col-md-4">
                        <label for="selectCollection" class="control-label">Select Collection <span class="text-danger">*</span></label>
                        <select class="form-control" name="collected_from" id="selectCollection" style="width: 100%;" required>
                            <option>Select Collection Status</option>
                            <option value="1" @if(isset($client)) @if($client->collected_from == 1) selected @endif @endif>MyEG</option>
                            <option value="2" @if(isset($client)) @if($client->collected_from == 2) selected @endif @endif>RTK.1</option>
                            <option value="3" @if(isset($client)) @if($client->collected_from == 3) selected @endif @endif>RTK.2</option>
                            <option value="4" @if(isset($client)) @if($client->collected_from == 4) selected @endif @endif>Calling</option>
                            <option value="5" @if(isset($client)) @if($client->collected_from == 5) selected @endif @endif>Others</option>
                        </select>
                        @error('collected_from')
                        <span class="help-block text-danger" style="color: red !important;">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="col-md-4">
                        <div class="form-group @error('country_id') has-error @enderror">
                            <label for="selectCountry" class="control-label">Select Contry <span class="text-danger">*</span></label>
                            <select class="form-control select2" name="country_id" id="selectCountry" style="width: 100%;" required>
                                @if(!isset($client))
                                <option>Select Country</option>
                                @endif
                                @foreach($allCountries as $country)
                                <option value="{{ $country->id }}" @if(isset($client)) @if($country->id == $client->country_id) selected @endif @endif>{{ $country->country_name }}</option>
                                @endforeach
                            </select>
                            @error('country')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group @error('date_of_birth') has-error @enderror">
                            <label for="dateOfBirth" class="control-label">Date of Birth <span class="text-danger">*</span></label>
                            <input type="text" name="date_of_birth" class="form-control datepicker" id="dateOfBirth" value="{{ isset($client) ? date('d/m/Y', strtotime($client->date_of_birth)) : old('date_of_birth') }}" placeholder="Enter Date of Birth" required>
                            @error('date_of_birth')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group @error('passport_issue_date') has-error @enderror">
                            <label for="passportIssueDate" class="control-label">Passport Issue Date <span class="text-danger">*</span></label>
                            <input type="text" name="passport_issue_date" class="form-control datepicker" id="passportIssueDate" value="{{ isset($client) ? date('d/m/Y', strtotime($client->passport_issue_date)) : old('passport_issue_date') }}" placeholder="Passport Issue Date" required>
                            @error('passport_issue_date')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group @error('passport_expire_date') has-error @enderror">
                            <label for="passportExpireDate" class="control-label">Passport Expire Date <span class="text-danger">*</span></label>
                            <input type="text" name="passport_expire_date" class="form-control datepicker" id="passportExpireDate" value="{{ isset($client) ? date('d/m/Y', strtotime($client->passport_expire_date)) : old('passport_expire_date') }}" placeholder="Passport Issue Date" required>
                            @error('passport_expire_date')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group @error('net_amount') has-error @enderror">
                            <label for="netAmount" class="control-label">Charge Amount <span class="text-danger">*</span></label>
                            <input type="number" name="net_amount" class="form-control" id="netAmount" value="{{ isset($client) ? $client->net_amount : old('net_amount') }}" placeholder="Charge Amount" required>
                            @error('net_amount')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group @error('discount_amount') has-error @enderror">
                            <label for="discountAmount" class="control-label">Discount Amount </label>
                            <input type="number" name="discount_amount" class="form-control" id="discountAmount" value="{{ isset($client) ? $client->discount_amount : old('discount_amount') }}" placeholder="Discount Amount">
                            @error('discount_amount')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group @error('address') has-error @enderror">
                            <label for="clientAddress" class="control-label">Address <span class="text-danger">*</span></label>
                            <textarea class="form-control" name="address" rows="3" id="clientAddress" placeholder="Enter Address" required>{{ isset($client) ? $client->address : old('address') }}</textarea>
                            @error('address')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group @error('comments') has-error @enderror">
                            <label for="clientComments" class="control-label">Comments</label>
                            <textarea class="form-control" name="comments" rows="3" id="clientComments" placeholder="Enter Comments">{{ isset($client) ? $client->comments : old('comments') }}</textarea>
                            @error('comments')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <a href="{{ route('malaysia-clients.index') }}" class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-info pull-right">@if(isset($client)) Malaysia Client Update Client @else Malaysia Client Create Client @endif</button>
            </div>
        </form>
    </div>
</section>
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
@endsection

@section('scripts')
<script src="{{ asset('vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
{{-- <script src="{{ asset('plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('plugins/input-mask/jquery.inputmask.extensions.js') }}"></script> --}}
<script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
<script>
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        todayHighlight: true,
        autoclose: true
    });
    // $('[data-mask]').inputmask();
    // $('.select2').select2();
    // $('.select2').find(':selected').data('Italy');

    $('.select2').select2({
        // ...
        templateSelection: function(data, container) {
            // Add custom attributes to the <option> tag for the selected option
            $(data.element).attr('data-custom-attribute', data.customValue);
            return data.text;
        }
    });
    $('.select2').find(':selected').data('Italy');
</script>
@endsection