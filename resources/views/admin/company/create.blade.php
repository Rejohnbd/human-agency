@extends('layouts.layout')

@if(isset($company))
@section('title', 'Update Company')
@else
@section('title', 'Create Cpmpany')
@endif

@section('content')

@component('partials.breadcrumb', [
'title' => isset($company) ? 'Update Company' : 'Create Company',
'activePage' => isset($company) ? 'Update Company' : 'Create Company'
])
@endcomponent

<section class="content">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">@if(isset($company)) Update Company Form @else Create Company Form @endif</h3>
        </div>
        <form class="form-horizontal" action="{{ isset($company) ? route('companies.update', $company->id) : route('companies.store') }}" method="POST">
            @csrf
            @if(isset($company))
            @method('PUT')
            @endif
            <div class="box-body">
                <div class="form-group @error('company_name') has-error @enderror">
                    <label for="companyName" class="col-sm-2 control-label">Company Name <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                        <input type="text" name="company_name" class="form-control" id="companyName" value="{{ isset($company) ? $company->company_name : old('company_name') }}" placeholder="Enter Company Name" required>
                        @error('company_name')
                        <span class="help-block text-danger">{{ $message }}</span>
                        @enderror
                        @error('company_name_slug')
                        <span class="help-block text-danger" style="color: red !important;">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <a href="{{ route('companies.index') }}" class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-info pull-right">@if(isset($company)) Update Company @else Create Company @endif</button>
            </div>
        </form>
    </div>
</section>
@endsection