<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', 'Auth\LoginController@showLoginForm');
Route::get('/', 'HomeController@index');

Auth::routes([
    'register'  => false,
    'reset'     => false,
    'verify'    => false
]);

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['auth', 'admin', 'prevent-back-history'])->group(function () {
    Route::get('admin-dashboard', 'AdminDashboardController@index')->name('admin-dashboard');
    Route::resource('users', 'UserController');
    Route::resource('clients', 'AdminClientController');
    Route::post('admin-clients-search', 'AdminClientController@adminClientsSearch')->name('admin-clients-search');
    Route::get('clients-import', 'AdminClientController@clientImport')->name('clients.import');
    Route::post('clients-import', 'AdminClientController@clientImportSave')->name('clients.import');
    Route::post('client_payment', 'AdminClientController@clientPayment')->name('client-payment');
    Route::get('invoice_print/{id}', 'AdminClientController@invoicePrint')->name('invoice-print');
    Route::get('invoice_download/{id}', 'AdminClientController@invoicePdf')->name('invoice-download');
    Route::post('document_upload', 'AdminClientController@documentUpload')->name('document-upload');
    Route::resource('embassy-approve', 'AdminEmbassyApproveController');
    Route::post('embassy-approve-list', 'AdminEmbassyApproveController@embassyApproveList')->name('embassy-approve-list');
    Route::get('embassy-approve-import', 'AdminEmbassyApproveController@embassyApproveImport')->name('embassy-approve-import');
    Route::post('embassy-approve-import', 'AdminEmbassyApproveController@embassyApproveSave')->name('embassy-approve-import');
    Route::resource('medical-status', 'AdminMedicalStatus');
    Route::resource('malaysia-clients', 'MalaysiaClientController');
    Route::resource('hiaring-moods', 'AdminHiaringMoodController');
    Route::resource('companies', 'AdminCompanyController');
});

Route::middleware(['auth', 'editor', 'prevent-back-history'])->group(function () {
    Route::get('editor-dashboard', 'EditorDashboardController@index')->name('editor-dashboard');
    Route::resource('editor-clients', 'EditorClientController');
    Route::post('editor-clients-search', 'EditorClientController@editorClientsSearch')->name('editor-clients-search');
    Route::post('editor_client_payment', 'EditorClientController@clientPayment')->name('editor-client-payment');
    Route::get('editor_invoice_print/{id}', 'EditorClientController@invoicePrint')->name('editor-invoice-print');
    Route::get('editor_invoice_download/{id}', 'EditorClientController@invoicePdf')->name('editor-invoice-download');
    Route::post('editor_document_upload', 'EditorClientController@documentUpload')->name('editor-document-upload');
});

Route::middleware(['auth', 'viewer', 'prevent-back-history'])->group(function () {
    Route::get('viewer-dashboard', 'ViewerDashboardController@index')->name('viewer-dashboard');
});

Route::get('migrate/{key}', function ($key) {
    if ($key == 'Rejohn@1234') {
        try {
            \Artisan::call('migrate');
            echo 'Migrated Successfully!';
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    } else {
        echo 'Not matched!';
    }
});


Route::get('clear', function () {
    \Artisan::call('optimize:clear');
    \Artisan::call('cache:clear');
    \Artisan::call('config:cache');
    \Artisan::call('config:clear');
    echo "Run clear Successfully";
});
