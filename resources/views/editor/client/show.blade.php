@extends('layouts.layout')

@section('title', 'Client Details')

@section('content')

@component('partials.breadcrumb',[
'title' => 'Details of: ' . $client->first_name . ' ' . $client->last_name,
'itemOne' => 'Client List',
'itemOneUrl' => 'clients.index',
'activePage' => 'Client Details'
])
@endcomponent

<section class="content">
    <div class="row">
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Client Info</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive img-circle" src="{{ asset('img/default.png') }}" alt="{{ $client->first_name . ' ' . $client->last_name}}">
                    <h3 class="profile-username text-center">{{ $client->first_name . ' ' . $client->last_name}}</h3>
                    <p class="text-muted text-center">{{ $client->country_name }}</p>
                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>NID</b> <a class="pull-right">{{ $client->nid }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Passport No</b> <a class="pull-right">{{ $client->passport_no }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>BMET Reg</b> <a class="pull-right">{{ $client->bmet_reg_no }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Phone Number</b> <a class="pull-right">{{ $client->mobile }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Phone Number (Opt)</b> <a class="pull-right">{{ $client->mobile_optional }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Home Number</b> <a class="pull-right">{{ $client->home_mobile }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Date of Birth</b> <a class="pull-right">{{ date('d-m-Y', strtotime($client->date_of_birth ))}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Passport Issue Date</b> <a class="pull-right">{{ date('d-m-Y', strtotime($client->passport_issue_date)) }}</a>
                        </li>
                        <li class="list-group-item" style="background-color: {{ $client->passportExpiredInfo->bg_color }};">
                            <b>Passport Expire Date</b> <a class="pull-right">{{ date('d-m-Y', strtotime($client->passport_expire_date)) }}</a>
                            <br />
                            <span class="pull-right {{ $client->passportExpiredInfo->text_class }}">{{ $client->passportExpiredInfo->date_str }}</span>
                            <br />
                        </li>
                        <li class="list-group-item">
                            <b>Visa Number</b> <a class="pull-right">{{ $client->visa_number }}</a>
                        </li>
                        <li class="list-group-item" style="background-color: {{ $client->visaExpiredInfo->bg_color }};">
                            <b>Visa Expire Date</b> <a class="pull-right">{{ isset($client->visa_expire_date) ? date('d/m/Y', strtotime($client->visa_expire_date)) : '' }}</a>
                            <br />
                            <span class="pull-right {{ $client->visaExpiredInfo->text_class }}">{{ $client->visaExpiredInfo->date_str }}</span>
                            <br />
                        </li>
                        <li class="list-group-item">
                            <b>Net Amount</b> <a class="pull-right">{{ $client->net_amount }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Discount Amount</b> <a class="pull-right">{{ $client->discount_amount }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Paid Amount</b> <a class="pull-right">{{ $client->paid_amount }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Total Pay</b> <a class="pull-right"><span id="tPaySum1">{{ $client->total_amount }}</span></a>
                        </li>
                        <li class="list-group-item">
                            <b>Created Date</b> <a class="pull-right">{{ date('d-m-Y', strtotime($client->created_at)) }}</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Extra Info</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <strong><i class="fa fa-book margin-r-5"></i> Comments</strong>
                    <p class="text-muted">
                        {{ $client->comments }}
                    </p>
                    <hr>
                    <strong><i class="fa fa-map-marker margin-r-5"></i> Address</strong>
                    <p class="text-muted">{{ $client->address }}</p>
                    <hr>
                    <strong><i class="fa fa-pencil margin-r-5"></i> Added By</strong>
                    <p>
                        <span class="label label-success">Admin</span>
                    </p>
                    {{-- <hr>
                    <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p> --}}
                </div>
            </div>
        </div>

        <div class="col-md-9">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#documents" data-toggle="tab">Documents</a></li>
                    <li><a href="#payments" data-toggle="tab">Payments</a></li>
                    <li><a href="#addDocument" data-toggle="tab">Add Document</a></li>
                    <li><a href="#addPayment" data-toggle="tab">Add Payment</a></li>
                </ul>
                <div class="tab-content">
                    <div class="active tab-pane" id="documents">
                        <div class="box-footer">
                            <ul class="mailbox-attachments clearfix">
                                <li>
                                    <span class="mailbox-attachment-icon has-img"><img src="../../dist/img/photo2.png" alt=""></span>
                                    <div class="mailbox-attachment-info">
                                        <a href="#" class="mailbox-attachment-name"><i class="fa fa-camera"></i> photo2.png</a>
                                        <span class="mailbox-attachment-size">
                                            1.9 MB
                                            <a href="#" class="btn btn-default btn-xs pull-right"><i class="fa fa-cloud-download"></i></a>
                                        </span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-pane" id="payments">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Payment List (Total Pay: <span id="tPaySum2">{{ $client->total_amount }}</span>)</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn bg-teal btn-sm"><i class="fa fa-cloud-download"></i></button>
                                </div>
                            </div>
                            <div class="box-body table-responsive no-padding">
                                <table id="paymenListTable" class="table table-hover">
                                    <tr>
                                        <th>SL</th>
                                        <th>Date</th>
                                        <th>Amount</th>
                                        <th>Received By</th>
                                        <th>Comment</th>
                                        <th>Action</th>
                                    </tr>
                                    @forelse($clientPayments as $payment)
                                    <tr id="{{$loop->iteration}}">
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ date('d-m-Y', strtotime($payment->payment_date)) }}</td>
                                        <td>{{ $payment->amount }}</td>
                                        <td><span class="label @if($payment->user->role->slug === 'admin') label-success @elseif ($payment->user->role->slug === 'editor') label-info @else label-warning @endif">{{ $payment->user->first_name . ' ' . $payment->user->last_name }}</span></td>
                                        <td>{{ $payment->comments }}</td>
                                        <td>
                                            <a href="{{ route('editor-invoice-print', $payment->id ) }}" target="_blank" class="btn bg-primary btn-sm"><i class="fa fa-print"></i></a>
                                            <a href="{{ route('editor-invoice-download', $payment->id ) }}" target="_blank" class="btn bg-info btn-sm"><i class="fa fa-cloud-download"></i></a>
                                        </td>
                                    </tr>
                                    @empty
                                    <tr id="0">
                                        <td colspan="6" class="text-center">No Payment Yet.</td>
                                    </tr>
                                    @endforelse
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="addDocument">
                        <form class="form-horizontal" action="{{ route('editor-document-upload') }}" enctype="multipart/form-data" method="POST">
                            @csrf
                            <input type="hidden" name="client_id" class="form-control" value="{{$client->id}}">
                            <div class="form-group">
                                <label for="docPicture" class="col-sm-2 control-label">Picture</label>
                                <div class="col-sm-3">
                                    <input type="file" name="picture" class="form-control" id="docPicture">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="docNID" class="col-sm-2 control-label">NID</label>
                                <div class="col-sm-3">
                                    <input type="file" name="nid" class="form-control" id="docNID">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="docPassport" class="col-sm-2 control-label">Passport</label>
                                <div class="col-sm-3">
                                    <input type="file" name="passport" class="form-control" id="docPassport">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button id="documentUpload" type="submit" class="btn btn-primary pull-right">Upload Document</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane" id="addPayment">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <input type="hidden" id="clientId" value="{{ $client->id }}">
                                <label for="paymentAmount" class="col-sm-2 control-label">Payment Amount</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control" id="paymentAmount" placeholder="Payment Amount" required>
                                    <span class="help-block text-danger" style="color: red" id="payment_amount"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="paymentComments" class="col-sm-2 control-label">Payment Comments</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" id="paymentComments" placeholder="Payment Comments"></textarea>
                                    <span class="help-block text-danger" style="color: red" id="payment_comments"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="paymentDate" class="col-sm-2 control-label">Payment Date</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control datepicker" id="paymentDate" placeholder="Payment Date" required>
                                    <span class="help-block text-danger" style="color: red" id="payment_date"></span>
                                </div>
                            </div>
                            <div class=" form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button id="savePayment" type="submit" class="btn btn-primary pull-right">Save Payment</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendors/sweet-alert/sweetalert.css') }}">
<style>
    .datepicker-orient-top {
        z-index: 999999 !important;
    }

    .no-blink {
        font-weight: bold;
        color: green;
    }

    .blink {
        font-weight: bold;
        color: red;
        animation: blinker 1s linear infinite;
    }

    @keyframes blinker {
        50% {
            opacity: 0;
        }
    }
</style>
@endsection

@section('scripts')
<script src="{{ asset('vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('vendors/sweet-alert/sweetalert.js') }}"></script>
<script>
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        todayHighlight: true,
        autoclose: true,
        zIndex: 9999999
    });

    $('#savePayment').on('click', function(e) {
        e.preventDefault();
        $('#payment_amount').text('');
        $('#payment_comments').text('');
        $('#payment_date').text('');

        let clientId = $('#clientId').val();
        let paymentAmount = $('#paymentAmount').val();
        let paymentComments = $('#paymentComments').val();
        let paymentDate = $('#paymentDate').val();
        let lastTrId = $('#paymenListTable tr:last').attr('id');

        $.ajax({
            url: "{{route('editor-client-payment')}}",
            type: "POST",
            data: {
                "_token": "{{ csrf_token() }}",
                client_id: clientId,
                payment_amount: paymentAmount,
                payment_comments: paymentComments,
                payment_date: paymentDate,
                last_tr_id: lastTrId,
            },
            success: function(response) {
                if (response.status == 201) {
                    if (response.tr_id == 0) {
                        $('#paymenListTable tr:last').remove();
                    }
                    $('#paymentAmount').val('');
                    $('#paymentComments').val('');
                    $('#paymentDate').val('');
                    $('#tPaySum1').text(response.total_amount);
                    $('#tPaySum2').text(response.total_amount);
                    $('#paymenListTable tr:last').after(response.payment_table_row);
                    Swal.fire(
                        'Good job!',
                        response.message,
                        'success'
                    );
                }

                if (response.status == 304) {
                    Swal.fire({
                        title: "Alert",
                        text: response.message,
                        icon: "error",
                        showCancelButton: true,
                        confirmButtonText: 'Exit',
                        cancelButtonText: 'Stay on the page'
                    });
                }
            },
            error: function(response) {
                $('#payment_amount').text(response.responseJSON.errors.payment_amount);
                $('#payment_comments').text(response.responseJSON.errors.payment_comments);
                $('#payment_date').text(response.responseJSON.errors.payment_date);
            }
        });
    });

    // $('#documentUpload').on('click', function(e) {
    //     e.preventDefault();
    // })
</script>
@if(session('success'))
<script>
    $(document).ready(function() {
        Swal.fire(
            'Good job!',
            "{{ session('success') }}",
            'success'
        );
    })
</script>
@endif
@if(session('error'))
<script>
    $(document).ready(function() {
        Swal.fire({
            title: "Alert",
            text: "{{ session('error') }}",
            icon: "error",
            showCancelButton: true,
            confirmButtonText: 'Exit',
            cancelButtonText: 'Stay on the page'
        });
    });
</script>
@endif
@endsection