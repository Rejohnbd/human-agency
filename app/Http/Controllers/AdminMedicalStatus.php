<?php

namespace App\Http\Controllers;

use App\Models\MedicalStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Validator;

class AdminMedicalStatus extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = MedicalStatus::all();
        return view('admin.medical_status.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.medical_status.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make(
            $request->all(),
            [
                'name'     => 'required|string',
            ],
            [
                'name.required'    => 'Medical Status Required',
                'name.string'      => 'Provide Vaild Medical Status',
            ]
        );

        if ($validate->fails()) {
            return back()->withErrors($validate->errors())->withInput();
        }

        $slug = Str::slug($request->name);
        $checkExist = MedicalStatus::where('slug', $slug)->exists();

        if ($checkExist) {
            session()->flash('error', 'This Medical Status Name Already Exists');
            return redirect()->back();
        } else {
            MedicalStatus::create([
                'slug' => $slug,
                'name' => $request->name
            ]);

            session()->flash('success', 'Medical Status Created Successfully.');
            return redirect()->route('medical-status.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(MedicalStatus $medicalStatus)
    {
        return view('admin.medical_status.create', compact('medicalStatus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MedicalStatus $medicalStatus)
    {
        $validate = Validator::make(
            $request->all(),
            [
                'name'     => 'required|string',
            ],
            [
                'name.required'    => 'Medical Status Required',
                'name.string'      => 'Provide Vaild Medical Status',
            ]
        );

        if ($validate->fails()) {
            return back()->withErrors($validate->errors())->withInput();
        }

        $slug = Str::slug($request->name);
        $checkExist = MedicalStatus::where('slug', $slug)->exists();

        if ($checkExist) {
            session()->flash('error', 'This Medical Status Name Already Exists');
            return redirect()->back();
        } else {
            $medicalStatus->update([
                'slug' => $slug,
                'name' => $request->name
            ]);

            session()->flash('success', 'Medical Status Created Successfully.');
            return redirect()->route('medical-status.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
