<?php

namespace App\Http\Middleware;

use App\Models\MedicalStatus;
use Closure;

class CheckMedicalStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (MedicalStatus::all()->count() === 0) {
            session()->flash('error', 'Need to Create Medical Stauts First');
            return redirect()->route('medical-status.index');
        }
        return $next($request);
    }
}
