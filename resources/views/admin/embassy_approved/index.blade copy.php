@extends('layouts.layout')

@section('title', 'Embassy Approved List')

@section('content')

@component('partials.breadcrumb',[
'title' => 'Embassy Approved',
'activePage' => 'Embassy Approved List'
])
@endcomponent

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <i class="fa fa-users"></i>
                    <h3 class="box-title">Embassy Approved Client List</h3>
                    <div class="box-tools">
                        <a href="{{ route('embassy-approve.create') }}" type="button" class="btn btn-info btn-flat btn-xs pull-right">Add Client</a>
                    </div>
                    <div class="box-tools">
                        <div class="input-group input-group-sm hidden-xs" style="width: 150px; margin-right: 10px">
                            <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="box-tools m">
                        <div class="input-group input-group-sm hidden-xs" style="width: 150px; margin-right: 10px">
                            <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                        </div>
                    </div>
                    <div class="box-tools">
                        <div class="input-group input-group-sm hidden-xs" style="width: 150px; margin-right: 10px">
                            <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">
                        </div>
                    </div>
                </div>
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th>#</th>
                                <th>Passport No</th>
                                <th>Name</th>
                                <th>Country</th>
                                <th>Phone</th>
                                <th>Passport Expire Date</th>
                                <th>Registration Date</th>
                                <th>Action</th>
                            </tr>
                            @forelse ($datas as $data)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $data->passport_no }}</td>
                                <td>{{ $data->name }}</td>
                                <td>{{ $data->country->country_name }}</td>
                                <td>{{ $data->mobile }}</td>
                                <td>{{ $data->passport_expire_date ? date('d/m/Y', strtotime($data->passport_expire_date)) : '' }}</td>
                                <td>{{ date('d/m/Y', strtotime($data->registration_date)) }}</td>
                                <td>
                                    <a href="{{ route('embassy-approve.edit', $data->id) }}" type="button" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Edit {{ $data->first_name }} {{ $data->last_name }} info"><i class="fa fa-edit"></i></a>
                                    <a href="#" type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete {{ $data->first_name }} {{ $data->last_name }}"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="8" class="text-center">No Client Created.</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                    <div class="text-center">
                        <ul class="pagination pagination-sm no-margin">
                            {!! $datas->render() !!}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('vendors/sweet-alert/sweetalert.css') }}">
@endsection

@section('scripts')
<script src="{{ asset('vendors/sweet-alert/sweetalert.js') }}"></script>
@if(session('success'))
<script>
    $(document).ready(function() {
        Swal.fire(
            'Good job!',
            "{{ session('success') }}",
            'success'
        );
    })
</script>
@endif

@if(session('error'))
<script>
    $(document).ready(function() {
        Swal.fire({
            title: "Alert",
            text: "{{ session('error') }}",
            icon: "error",
            showCancelButton: true,
            confirmButtonText: 'Exit',
            cancelButtonText: 'Stay on the page'
        });
    });
</script>
@endif
@endsection