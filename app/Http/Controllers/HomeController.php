<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::user()->role->id == 1) {
            // return view('home');
            return redirect()->route('admin-dashboard');
        } else if (Auth::user()->role->id == 2) {
            return redirect()->route('editor-dashboard');
        } else if (Auth::user()->role->id == 3) {
            return redirect()->route('viewer-dashboard');
        } else {
            return redirect()->route('login');
        }
    }
}
