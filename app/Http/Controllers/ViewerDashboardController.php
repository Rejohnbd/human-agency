<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ViewerDashboardController extends Controller
{
    public function index()
    {
        return view('viewer.dashboard.index');
    }
}
