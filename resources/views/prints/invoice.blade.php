@extends('layouts.print-layout')

@section('title', 'Invoice Print')

@section('content')
<section class="invoice">
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header">
                JHT Global
                <small class="pull-right">Date: {{ date('d/m/Y') }}</small>
            </h2>
        </div>
    </div>
    <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
            From
            <address>
                <strong>JHT Global</strong><br>
                House # 12, Road # 12/A<br>
                Dhanmondi, Dhaka<br>
                Phone: 01717 546 5XX<br>
                Email: info@jhtglobal.com
            </address>
        </div>
        <div class="col-sm-4 invoice-col">
            To
            <address>
                <strong>{{ $clientInfo->first_name . ' '. $clientInfo->last_name }}</strong><br>
                {{ $clientInfo->address }}<br>
                Phone: {{ $clientInfo->mobile }}<br>
            </address>
        </div>
        <div class="col-sm-4 invoice-col">
            <b>Invoice # {{ $invoiceInfo->invoice_no }}</b><br>
            <b>Payment Date:</b> {{ date('d-m-Y', strtotime($invoiceInfo->payment_date)) }}<br>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Amount</th>
                        <th>Comment</th>
                        <th>Received By</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ date('d-m-Y', strtotime($invoiceInfo->payment_date)) }}</td>
                        <td>{{ $invoiceInfo->amount }}</td>
                        <td>{{ $invoiceInfo->comments }}</td>
                        <td>{{ $clientInfo->user->first_name . ' ' . $clientInfo->user->last_name }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                Hello {{ $clientInfo->first_name . ' '. $clientInfo->last_name }}, <br />
                Received with thanks amount {{ $invoiceInfo->amount }} Taka. <br />
                <br />
                Have a great day.</br>
                JHT Global
            </p>
        </div>
    </div>
</section>
</div>

@endsection