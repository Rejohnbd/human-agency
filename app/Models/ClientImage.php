<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientImage extends Model
{
    protected $fillable = [
        'client_id', 'document_name', 'document_path', 'file_name', 'file_blob'
    ];
}
