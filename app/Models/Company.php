<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'companies';


    public function clients()
    {
        return $this->hasMany(Client::class);
    }

    public function editors()
    {
        return $this->hasMany(User::class)->where('role_id', '=', 2);
    }
}
