@extends('layouts.layout')

@section('title', 'Client List')

@section('content')

@component('partials.breadcrumb',[
'title' => 'Client List',
'activePage' => 'Client List'
])
@endcomponent
<section class="content">
    <div class="row">
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-search"></i></span>
                <div class="info-box-content">
                    <h4>Search By Name</h4>
                    <span class="info-box-number">
                        <input id="nameSearch" class="form-control client-search" placeholder="Search By Name" />
                    </span>
                </div>
            </div>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-search"></i></span>
                <div class="info-box-content">
                    <h4>Search By Phone</h4>
                    <span class="info-box-number">
                        <input id="phoneSearch" class="form-control client-search" placeholder="Search By Phone" />
                    </span>
                </div>
            </div>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa fa-search"></i></span>
                <div class="info-box-content">
                    <h4>Search By Passport</h4>
                    <span class="info-box-number">
                        <input id="passportSearch" class="form-control client-search" placeholder="Search By Passport" />
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <i class="fa fa-users"></i>
                    <h3 class="box-title">Client List</h3>
                    <div class="box-tools">
                        <a href="{{ route('clients.create') }}" type="button" class="btn btn-info btn-flat btn-xs pull-right">Add Client</a>
                    </div>
                </div>
                <div class="box-body table-responsive no-padding" id="clientTable">
                    @include('admin.client.client_render')
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('vendors/sweet-alert/sweetalert.css') }}">
<style>
    .no-blink {
        font-weight: bold;
        color: green;
    }

    .blink {
        font-weight: bold;
        color: red;
        animation: blinker 1s linear infinite;
    }

    @keyframes blinker {
        50% {
            opacity: 0;
        }
    }
</style>
@endsection

@section('scripts')
<script src="{{ asset('vendors/sweet-alert/sweetalert.js') }}"></script>
<script>
    $(document).ready(function() {

        $('.client-search').keyup(function(e) {
            let nameSearch = $('#nameSearch').val();
            let phoneSearch = $('#phoneSearch').val();
            let passportSearch = $('#passportSearch').val();

            $.ajax({
                type: "post",
                url: "{{ url('admin-clients-search')}}",
                data: {
                    name: nameSearch,
                    mobile: phoneSearch,
                    passport: passportSearch,
                    _token: '{{ csrf_token() }}'
                },
                // dataType: "json",
                success: function(datas) {
                    $('#clientTable').html(datas);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR, textStatus, errorThrown);
                    Swal.fire({
                        title: "Alert",
                        text: "Not Working. Please Contact to Author",
                        icon: "error",
                        showCancelButton: true,
                        confirmButtonText: 'Exit',
                        cancelButtonText: 'Stay on the page'
                    });
                }
            });
        });
    });
</script>

@if(session('success'))
<script>
    $(document).ready(function() {
        Swal.fire(
            'Good job!',
            "{{ session('success') }}",
            'success'
        );
    })
</script>
@endif

@if(session('error'))
<script>
    $(document).ready(function() {
        Swal.fire({
            title: "Alert",
            text: "{{ session('error') }}",
            icon: "error",
            showCancelButton: true,
            confirmButtonText: 'Exit',
            cancelButtonText: 'Stay on the page'
        });
    });
</script>
@endif
@endsection