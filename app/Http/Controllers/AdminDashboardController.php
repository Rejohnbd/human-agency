<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Company;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AdminDashboardController extends Controller
{
    public function index()
    {
        $fromdate = Carbon::now()->toDateString();
        $todate = Carbon::now()->addDays(60)->toDateString();

        $data = User::all();
        $allAdmins = $data->where('role_id', 1)->count();
        $allEditors = $data->where('role_id', 2)->count();
        $allVisitors = $data->where('role_id', 3)->count();

        $allClients = Client::all()->count();
        $nearToPassportExpireClient = Client::whereBetween('passport_expire_date', [$fromdate, $todate])->get()->count();
        $passportExpiredClient = Client::whereDate('passport_expire_date', '<', $fromdate)->get()->count();

        $numCom = Company::all()->count();
        return view('admin.dashboard.index', compact('allClients', 'nearToPassportExpireClient', 'passportExpiredClient', 'allAdmins', 'allEditors', 'allVisitors', 'numCom'));
    }
}
