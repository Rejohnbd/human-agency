<?php

namespace App\Http\Requests\Client;

use Illuminate\Foundation\Http\FormRequest;

class ClientPaymentStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_id'         => 'required|numeric',
            'payment_amount'    => 'required|numeric',
            'payment_date'      => 'required|date_format:d/m/Y',
        ];
    }

    public function messages()
    {
        return [
            'payment_amount.required'   => 'Payment Amount Required',
            'payment_amount.numeric'    => 'Payment Amount Only Number',
            'payment_date.required'     => 'Payment Date Required',
            'payment_date.date_format'  => 'Provide Valid Payment Date Format',
        ];
    }
}
