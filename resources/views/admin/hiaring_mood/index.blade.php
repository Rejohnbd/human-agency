@extends('layouts.layout')

@section('title', 'Hiaring Mood')

@section('content')

@component('partials.breadcrumb',[
'title' => 'Hiaring Mood List',
'activePage' => 'Hiaring Mood List'
])
@endcomponent

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <i class="fa fa-users"></i>
                    <h3 class="box-title">Hiaring Mood</h3>
                    <div class="box-tools">
                        <a href="{{ route('hiaring-moods.create') }}" type="button" class="btn btn-info btn-flat btn-xs pull-right">Add Hiaring Mood</a>
                    </div>
                </div>
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                            @forelse ($hmoods as $data)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $data->name }}</td>
                                <td>
                                    <a href="{{ route('hiaring-moods.edit', $data->id) }}" type="button" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Edit {{ $data->name }} info"><i class="fa fa-edit"></i></a>
                                    {{-- <a href="#" type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete {{ $data->name }}"><i class="fa fa-trash"></i></a> --}}
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="3" class="text-center">No Hiaring Mood.</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('vendors/sweet-alert/sweetalert.css') }}">
@endsection

@section('scripts')
<script src="{{ asset('vendors/sweet-alert/sweetalert.js') }}"></script>
@if(session('success'))
<script>
    $(document).ready(function() {
        Swal.fire(
            'Good job!',
            "{{ session('success') }}",
            'success'
        );
    })
</script>
@endif

@if(session('error'))
<script>
    $(document).ready(function() {
        Swal.fire({
            title: "Alert",
            text: "{{ session('error') }}",
            icon: "error",
            showCancelButton: true,
            confirmButtonText: 'Exit',
            cancelButtonText: 'Stay on the page'
        });
    });
</script>
@endif
@endsection