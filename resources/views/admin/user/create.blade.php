@extends('layouts.layout')

@if(isset($user))
@section('title', 'Create User')
@else
@section('title', 'Update User')
@endif

@section('content')

@component('partials.breadcrumb', [
'title' => isset($user) ? 'Update User' : 'Create User',
'activePage' => isset($user) ? 'Update User' : 'Create User'
])
@endcomponent

<section class="content">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">@if(isset($user)) Update User Form @else Create User Form @endif</h3>
        </div>
        <form class="form-horizontal" action="{{ isset($user) ? route('users.update', $user->id) : route('users.store') }}" method="POST">
            @csrf
            @if(isset($user))
            @method('PUT')
            @endif
            <div class="box-body">
                <div class="form-group @error('first_name') has-error @enderror">
                    <label for="firstName" class="col-sm-2 control-label">User First Name <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                        <input type="text" name="first_name" class="form-control" id="firstName" value="{{ isset($user) ? $user->first_name : old('first_name') }}" placeholder="Enter User First Name" required>
                        @error('first_name')
                        <span class="help-block text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group @error('last_name') has-error @enderror">
                    <label for="lastName" class="col-sm-2 control-label">User Last Name <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                        <input type="text" name="last_name" class="form-control" id="lastName" value="{{ isset($user) ? $user->last_name : old('last_name') }}" placeholder="Enter User Last Name" required>
                        @error('last_name')
                        <span class="help-block">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group @error('email') has-error @enderror">
                    <label for="userEmail" class="col-sm-2 control-label">User Email <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                        <input type="email" name="email" class="form-control" id="userEmail" value="{{ isset($user) ? $user->email : old('email') }}" placeholder="Enter User Email" required>
                        @error('email')
                        <span class="help-block">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group @error('phone') has-error @enderror">
                    <label for="userMobile" class="col-sm-2 control-label">User Mobile Number <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                        <input type="number" name="phone" class="form-control" id="userMobile" value="{{ isset($user) ? $user->phone : old('phone') }}" placeholder="Enter User Mobile Number (01XXXXXXXXX)" required>
                        @error('phone')
                        <span class="help-block">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group @error('company_id') has-error @enderror">
                    <label for="userCompany" class="col-sm-2 control-label">User Company <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                        <select class="form-control" id="userCompany" name="company_id" required>
                            <option value="0" @if(old('company_id') !=0) selected @endif>Select User Company</option>
                            @foreach ($companies as $company)
                            @if(isset($user))
                            <option @if(old('company_id', $user->company_id)==$company->id) selected @endif value="{{ $company->id }}">{{ $company->company_name }}</option>
                            @else
                            <option @if(old('company_id')==$company->id) selected @endif value="{{ $company->id }}">{{ $company->company_name }}</option>
                            @endif
                            @endforeach
                        </select>
                        @error('company_id')
                        <span class="help-block">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group @error('role_id') has-error @enderror">
                    <label for="userRole" class="col-sm-2 control-label">User Role <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                        <select class="form-control" id="userRole" name="role_id" required>
                            <option value="0" @if(old('role_id') !=0) selected @endif>Select User Role</option>
                            @foreach ($roles as $role)
                            @if(isset($user))
                            <option @if(old('role_id', $user->role_id)==$role->id) selected @endif value="{{ $role->id }}">{{ $role->name }}</option>
                            @else
                            <option @if(old('role_id')==$role->id) selected @endif value="{{ $role->id }}">{{ $role->name }}</option>
                            @endif
                            @endforeach
                        </select>
                        @error('role_id')
                        <span class="help-block">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group @error('password') has-error @enderror">
                    <label for="userPassword" class="col-sm-2 control-label">User Password <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                        <input type="password" name="password" class="form-control" id="userPassword" placeholder="Enter User Password" @if(isset($user)) @else required @endif>
                        @error('password')
                        <span class="help-block">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <label for="confirmPassword" class="col-sm-2 control-label">Confirm Password <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                        <input type="password" name="password_confirmation" class="form-control" id="confirmPassword" placeholder="Confirm Password" @if(isset($user)) @else required @endif>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <a href="{{ route('users.index') }}" class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-info pull-right">@if(isset($user)) Update User @else Create User @endif</button>
            </div>
        </form>
    </div>
</section>
@endsection