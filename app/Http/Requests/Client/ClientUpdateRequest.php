<?php

namespace App\Http\Requests\Client;

use Illuminate\Foundation\Http\FormRequest;

class ClientUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id'            => 'required|exists:companies,id',
            'first_name'            => 'required|string',
            'last_name'             => 'required|string',
            'nid'                   => 'required|string|unique:clients,nid,' . $this->client->id,
            'date_of_birth'         => 'required|date_format:d/m/Y',
            'passport_no'           => 'required|string|unique:clients,passport_no,' . $this->client->id,
            'passport_issue_date'   => 'required|date_format:d/m/Y',
            'passport_expire_date'  => 'required|date_format:d/m/Y',
            'mobile'                => 'required|unique:clients,mobile,' . $this->client->id,
            'mobile_optional'       => 'nullable',
            'home_mobile'           => 'nullable',
            'country_id'            => 'required|numeric',
            'net_amount'            => 'required|numeric',
            'medical_status'        => 'required|numeric',
            'hiaring_mood'          => 'required|numeric|exists:hiaring_moods,id',
            'discount_amount'       => 'nullable|numeric|lt:net_amount',
            'address'               => 'required|string',
            'comments'              => 'nullable|string',
            'visa_number'           => 'nullable|unique:clients,visa_number,' . $this->client->id,
            'visa_expire_date'      => 'nullable|date_format:d/m/Y'
        ];
    }

    public function messages()
    {
        return [
            'company_id.required'               => 'Comapny Select is Required',
            'company_id.exists'                 => 'Please Select Valid Company',
            'first_name.required'               => 'First Name is Required.',
            'first_name.string'                 => 'Provide Valid First Name.',
            'last_name.required'                => 'Last Name is Required.',
            'last_name.required'                => 'Provide Valid Last Name.',
            'nid.required'                      => 'National Id No Required.',
            'nid.string'                        => 'Provide Valid National Id No.',
            'date_of_birth.required'            => 'Date of Birth Required.',
            'date_of_birth.date'                => 'Provide Valid Date of Birth.',
            'passport_no.required'              => 'Passport No is Required.',
            'passport_no.string'                => 'Provide Valid Passport No.',
            'passport_no.unique'                => 'This Passport No Allready Exist.',
            'passport_issue_date.required'      => 'Passport Issue Date is Required.',
            'passport_issue_date.date'          => 'Provide Valid Passport Issue Date Format.',
            'passport_issue_date.date_format'   => 'Provide Valid Passport Issue Date Format.',
            'passport_expire_date.required'     => 'Passport Expire Date is Required.',
            'passport_expire_date.date'         => 'Provide Valid Passport Expire Date Format.',
            'passport_expire_date.date_format'  => 'Provide Valid Passport Expire Date Format.',
            'mobile.required'                   => 'Phone Number is Required.',
            'mobile.min'                        => 'Provide Valid Phone Number.',
            'mobile.max'                        => 'Provide Valid Phone Number.',
            'mobile.unique'                     => 'Phone Number is Allready Exist.',
            'mobile_optional.min'               => 'Provide Valid Phone Number Optional.',
            'mobile_optional.max'               => 'Provide Valid Phone Number Optional.',
            // 'home_mobile.required'              => 'Home Phone Number is Required.',
            'home_mobile.min'                   => 'Provide Valid Home Phone Number.',
            'home_mobile.max'                   => 'Provide Valid Home Phone Number.',
            'home_mobile.max'                   => 'Provide Valid Home Phone Number.',
            'country_id.required'               => 'Country Name is Required.',
            'country_id.numeric'                => 'Provide Valid Country Name.',
            'net_amount.required'               => 'Charge Amount is Required.',
            'net_amount.numeric'                => 'Provide Valid Charge Amount.',
            'medical_status.required'           => 'Medical Status Required',
            'medical_status.numeric'            => 'Provide Valid Medical Status',
            'discount_amount.numeric'           => 'Provide Valid Discount Amount.',
            'discount_amount.lt'                => 'Discount Amount Must be Lessthan Charge Amount.',
            'address.required'                  => 'Address is Required.',
            'address.string'                    => 'Provide Valid Address.',
            'hiaring_mood.required'             => 'Hiaring Mood Required',
            'hiaring_mood.numeric'              => 'Provide Valid Hiaring Mood',
            'hiaring_mood.exists'               => 'Provide Valid Hiaring Mood',
            'visa_number.unique'                => 'Visa Number Already Exists',
            'visa_expire_date.date_format'      => 'Provide Valid Visa Expire Date Format.'
        ];
    }
}
