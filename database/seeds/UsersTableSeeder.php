<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'role_id'       => 1,
            'first_name'    => 'Md.',
            'last_name'     => 'Admin',
            'email'         => 'admin@mail.com',
            'password'      => Hash::make('password'),
            'editable'      => 0,
            'deleteable'    => 0,
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s')
        ]);

        DB::table('users')->insert([
            'role_id'       => 2,
            'first_name'    => 'Md.',
            'last_name'     => 'Editor',
            'email'         => 'editor@mail.com',
            'password'      => Hash::make('password'),
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s')
        ]);

        DB::table('users')->insert([
            'role_id'       => 3,
            'first_name'    => 'Md.',
            'last_name'     => 'Viewer',
            'email'         => 'viewer@mail.com',
            'password'      => Hash::make('password'),
            'created_at'    => date('Y-m-d H:i:s'),
            'updated_at'    => date('Y-m-d H:i:s')
        ]);
    }
}
