<?php

namespace App\Http\Requests\EmbassyApprove;

use Illuminate\Foundation\Http\FormRequest;

class ApprovedClientUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'passport_no'           => 'required|string|unique:embassy_approves,passport_no,' . $this->embassyApprove->id,
            'name'                  => 'required|string',
            'country_id'            => 'required|numeric',
            'registration_date'     => 'required|date_format:d/m/Y'
        ];
    }

    public function messages()
    {
        return [
            'passport_no.required'          => 'Passport No is Required.',
            'passport_no.string'            => 'Provide Valid Passport No.',
            'passport_no.unique'            => 'This Passport No Allready Exist.',
            'name.required'                 => 'Name is Required.',
            'name.string'                   => 'Provide Valid Name.',
            'country_id.required'           => 'Country Name is Required.',
            'country_id.numeric'            => 'Provide Valid Country Name.',
            'registration_date.required'    => 'Passport Issue Date is Required.',
            'registration_date.date_format' => 'Provide Valid Passport Issue Date Format.',
        ];
    }
}
