<?php

namespace App\Http\Controllers;

use App\Http\Requests\Company\CompanyStoreRequest;
use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Validator;

class AdminCompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = Company::with(['clients', 'editors'])->get();
        return view('admin.company.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->request->add(['company_name_slug' => Str::slug($request->company_name)]);

        $validated = $request->validate(
            [
                'company_name'      => 'required|string',
                'company_name_slug' => 'unique:companies,company_name_slug'
            ],
            [
                'company_name.required'     => 'Company Name is Required.',
                'company_name.string'       => 'Provide Valid Company Name.',
                'company_name_slug.unique'  => 'Company Name Already Exist.',
            ]
        );

        $newCompany = new Company;
        $newCompany->company_name = $request->company_name;
        $newCompany->company_name_slug = Str::slug($request->company_name);
        $newCompany->save();

        session()->flash('success', 'Company Created Successfully.');
        return redirect()->route('companies.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        return view('admin.company.create', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        $request->request->add(['company_name_slug' => Str::slug($request->company_name)]);

        $validated = $request->validate(
            [
                'company_name'      => 'required|string',
                'company_name_slug' => 'unique:companies,company_name_slug,' . $company->id
            ],
            [
                'company_name.required'     => 'Company Name is Required.',
                'company_name.string'       => 'Provide Valid Company Name.',
                'company_name_slug.unique'  => 'Company Name Already Exist.',
            ]
        );

        $company->company_name = $request->company_name;
        $company->company_name_slug = Str::slug($request->company_name);
        $company->save();

        session()->flash('success', 'Company Updated Successfully.');
        return redirect()->route('companies.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
