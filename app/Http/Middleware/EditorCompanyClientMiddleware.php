<?php

namespace App\Http\Middleware;

use App\Models\Client;
use Closure;
use Illuminate\Support\Facades\Auth;

class EditorCompanyClientMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd($request->route()->parameters(), $request->route('editor_client')->id);
        if (Client::where('id', $request->route('editor_client')->id)->where('company_id', Auth::user()->company_id)->exists()) {
            return $next($request);
        } else {
            session()->flash('error', 'Something Happend Wrong');
            return redirect()->route('editor-clients.index');
        }
    }
}
