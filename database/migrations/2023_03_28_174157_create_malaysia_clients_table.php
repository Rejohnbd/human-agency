<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMalaysiaClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('malaysia_clients', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->comment('FK from table:user');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('passport_no', 30)->unique();
            $table->date('date_of_birth');
            $table->date('passport_issue_date');
            $table->date('passport_expire_date');
            $table->string('mobile', 20)->unique();
            $table->string('home_mobile');
            $table->string('country_id', 11);
            $table->decimal('net_amount', 10, 2)->nullable();
            $table->decimal('discount_amount', 10, 2)->nullable();
            $table->decimal('paid_amount', 10, 2)->nullable();
            $table->decimal('total_amount', 10, 2)->nullable();
            $table->text('address')->nullable();
            $table->text('comments')->nullable();
            $table->tinyInteger('medical_status')->default(0);
            $table->tinyInteger('edit_status')->default(0);
            $table->tinyInteger('delete_status')->default(0);
            $table->tinyInteger('completed_status')->default(0);
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('malaysia_clients');
    }
}
