<table class="table table-hover">
    <tbody>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Company</th>
            <th>Country</th>
            <th>Phone</th>
            <th>Passport No</th>
            <th>Passport Expire Date</th>
            <th>Visa Expire Date</th>
            <th>Net Amount</th>
            <th>Discount Amount</th>
            <th>Paid Amount</th>
            <th>Total Pay</th>
            <th>Created Date</th>
            <th>Created By</th>
            <th>Action</th>
        </tr>
        @forelse ($datas as $data)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $data->first_name }} {{ $data->last_name }}</td>
            <td>@if(isset($data->company->company_name)) {{ $data->company->company_name }} @endif</td>
            <td>{{ $data->country->country_name }}</td>
            <td>{{ $data->mobile }}</td>
            <td>{{ $data->passport_no }}</td>
            <td style="background-color: {{ $data->passportExpiredInfo->bg_color }};">
                {{ date('d/m/Y', strtotime($data->passport_expire_date)) }}
                <br />
                <span class="{{ $data->passportExpiredInfo->text_class }}">{{ $data->passportExpiredInfo->date_str }}</span>
            </td>
            <td style="background-color: {{ $data->visaExpiredInfo->bg_color }};">
                {{ isset($data->visa_expire_date) ? date('d/m/Y', strtotime($data->visa_expire_date)) : '' }}
                <br />
                <span class="{{ $data->visaExpiredInfo->text_class }}">{{ $data->visaExpiredInfo->date_str }}</span>
            </td>
            <td>{{ $data->net_amount }}</td>
            <td>{{ $data->discount_amount }}</td>
            <td>{{ $data->paid_amount }}</td>
            <td>{{ $data->total_amount }}</td>
            <td>{{ date('d/m/Y', strtotime($data->created_at)) }}</td>
            <td><span class="label @if($data->user->role->slug === 'admin') label-success @elseif ($data->user->role->slug === 'editor') label-info @else label-warning @endif">{{ $data->user->first_name }} {{ $data->user->last_name }}</span></td>
            <td>
                <a href="{{ route('clients.show', $data->id) }}" type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Show {{ $data->first_name }} {{ $data->last_name }} Details"><i class="fa fa-eye"></i></a>
                <a href="{{ route('clients.edit', $data->id) }}" type="button" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Edit {{ $data->first_name }} {{ $data->last_name }} info"><i class="fa fa-edit"></i></a>
                <a href="#" type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete {{ $data->first_name }} {{ $data->last_name }}"><i class="fa fa-trash"></i></a>
            </td>
        </tr>
        @empty
        <tr>
            <td colspan="12" class="text-center">No Client Created.</td>
        </tr>
        @endforelse
    </tbody>
</table>
<div class="text-center">
    <ul class="pagination pagination-sm no-margin">
        {!! $datas->render() !!}
    </ul>
</div>