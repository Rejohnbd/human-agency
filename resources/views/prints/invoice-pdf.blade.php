<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
</head>

<body>
    <section class="invoice">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    JHT Global
                    <small class="pull-right">Date: {{ date('d/m/Y') }}</small>
                </h2>
            </div>
        </div>
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                From
                <address>
                    <strong>JHT Global</strong><br>
                    House # 12, Road # 12/A<br>
                    Dhanmondi, Dhaka<br>
                    Phone: 01717 546 5XX<br>
                    Email: info@jhtglobal.com
                </address>
            </div>
            <div class="col-sm-4 invoice-col">
                To
                <address>
                    <strong>{{ $clientName }}</strong><br>
                    {{ $clientAddress}}<br>
                    Phone: {{ $clientMobile }}<br>
                </address>
            </div>
            <div class="col-sm-4 invoice-col">
                <b>Invoice # {{ $invoiceNo }}</b><br>
                <b>Payment Date:</b> {{ $paymentDate }}<br>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Amount</th>
                            <th>Comment</th>
                            <th>Received By</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{ $paymentDate }}</td>
                            <td>{{ $paymentAmount }}</td>
                            <td>{{ $paymentComment }}</td>
                            <td>{{ $clientName }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                    Hello {{ $clientName }}, <br />
                    Received with thanks amount {{ $paymentAmount }} Taka. <br />
                    <br />
                    Have a great day.</br>
                    JHT Global
                </p>
            </div>
        </div>
    </section>
    </div>

</body>

</html>