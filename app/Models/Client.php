<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id', 'id');
    }

    public function medicalStatus()
    {
        return $this->belongsTo(MedicalStatus::class, 'medical_status', 'id')->withDefault();
    }

    public function getPassportExpiredInfoAttribute()
    {

        $passExpireDate = Carbon::parse($this->passport_expire_date);
        if (Carbon::today()->lt($passExpireDate)) {
            $color = '';
            $textCls = 'no-blink';
            $monthDiff = Carbon::today()->diffInMonths($passExpireDate);

            if ($monthDiff < 3) {
                $color = '#eba7a7';
                $textCls = 'blink';
            }

            return (object) array(
                'bg_color'      => $color,
                'text_class'    => $textCls,
                'date_str'      => Carbon::parse($this->passport_expire_date)->diff(Carbon::now())->format('%y Years, %m Months and %d Days'),
            );
        } else {
            return (object) array(
                'bg_color'      => 'red',
                'text_class'    => '',
                'date_str'      => ''
            );
        }
    }

    public function getVisaExpiredInfoAttribute()
    {
        if (isset($this->visa_expire_date)) {
            $visaExpireDate = Carbon::parse($this->visa_expire_date);

            if (Carbon::today()->lt($visaExpireDate)) {
                $color = '';
                $textCls = 'no-blink';
                $monthDiff = Carbon::today()->diffInMonths($visaExpireDate);

                if ($monthDiff < 3) {
                    $color = '#eba7a7';
                    $textCls = 'blink';
                }

                return (object) array(
                    'bg_color'      => $color,
                    'text_class'    => $textCls,
                    'date_str'      => Carbon::parse($this->visa_expire_date)->diff(Carbon::now())->format('%y Years, %m Months and %d Days'),
                );
            } else {
                return (object) array(
                    'bg_color'      => 'red',
                    'text_class'    => '',
                    'date_str'      => ''
                );
            }
        } else {
            return (object) array(
                'bg_color'      => '',
                'text_class'    => '',
                'date_str'      => ''
            );
        }
    }
}
