<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'    => 'required|string',
            'last_name'     => 'required|string',
            'email'         => 'required|string|email|max:255|unique:users,email,' . $this->user->id,
            'phone'         => 'required|min:11|max:11|unique:users,phone,' . $this->user->id,
            'role_id'       => 'required|in:1,2,3',
            'company_id'    => 'required|exists:companies,id'
        ];
    }

    public function messages()
    {
        return [
            'first_name.required'   => 'First Name is Required.',
            'first_name.string'     => 'Provide Valid First Name.',
            'last_name.required'    => 'Last Name is Required.',
            'last_name.string'      => 'Provide Valid Last Name.',
            'email.required'        => 'Email Address is Required.',
            'email.string'          => 'Provide Valid Email Address.',
            'email.email'           => 'Provide Valid Email Address.',
            'email.max'             => 'Provide Valid Email Address.',
            'email.unique'          => 'Email Address Already Exist.',
            'phone.required'        => 'Phone Number is Required.',
            'phone.min'             => 'Provide valid Phone Number.',
            'phone.max'             => 'Provide valid Phone Number.',
            'phone.unique'          => 'Phone Number Already Exist.',
            'role_id.required'      => 'User Role is Required.',
            'role_id.in'            => 'Provide Valid User Role.',
            'company_id.required'   => 'Comapny Select is Required',
            'company_id.exists'     => 'Please Select Valid Company'
        ];
    }
}
