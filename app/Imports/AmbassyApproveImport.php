<?php

namespace App\Imports;

use App\Models\EmbassyApprove;
use Maatwebsite\Excel\Concerns\ToModel;

class AmbassyApproveImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new EmbassyApprove([
            'passport_no'           => $row[0],
            'name'                  => $row[1],
            'country_id'            => $row[2],
            'mobile'                => is_null($row[3]) ? null : $row[3],
            'passport_expire_date'  => is_null($row[4]) ? null : $row[3],
            'registration_date'     => $row[5]
        ]);
    }
}
