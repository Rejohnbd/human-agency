@extends('layouts.layout')

@section('title', 'Client Import')

@section('content')

@component('partials.breadcrumb',[
'title' => 'Import Client',
'activePage' => 'Client Import'
])
@endcomponent
<section class="content">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Import Client</h3>
            <div class="box-tools pull-right">
                <span class="label label-danger">Fillable & Formatted</span>
                &nbsp;
                <span class="label label-warning">Not Fillable & Formatted</span>
                &nbsp;
                <span class="label" style="color: #000; border: 1px solid green;">Not Fillable</span>
                &nbsp;
                <a href="{{ asset('demo/client.xlsx') }}" download type="button" class="btn btn-info btn-flat btn-xs pull-right">Demo XLSX</a>
            </div>
        </div>
        <form action="{{ route('clients.import') }}" enctype="multipart/form-data" method="POST">
            @csrf
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group @error('user_list_file') has-error @enderror">
                            <label for="userCsv" class="control-label">Upload Client list (.xlsx) <span class="text-danger">*</span></label>
                            <input type="file" name="user_list_file" accept=".xlsx" class="form-control" id="userCsv">
                            @error('user_list_file')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <a href="{{ route('clients.index') }}" class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-info pull-right">Upload</button>
            </div>
        </form>
    </div>
    @if(isset($errorsData))
    <div class="box box-danger">
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <tbody>
                    <tr>
                        <th>Row No</th>
                        <th>Errors</th>
                    </tr>
                    @foreach($errorsData as $data)
                    <tr>
                        <td>{{ $data['key'] + 4 }}</td>
                        <td>
                            @foreach($data['errors'] as $er)
                            <p class="text-danger">{{$er}}</p>
                            @endforeach
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @php
    session()->forget('errors_data');
    @endphp
    @endif
</section>
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('vendors/sweet-alert/sweetalert.css') }}">
@endsection

@section('scripts')
<script src="{{ asset('vendors/sweet-alert/sweetalert.js') }}"></script>
@if(session('success'))
<script>
    $(document).ready(function() {
        Swal.fire(
            'Good job!',
            "{{ session('success') }}",
            'success'
        );
    })
</script>
@endif

@if(session('error'))
<script>
    $(document).ready(function() {
        Swal.fire({
            title: "Alert",
            text: "{{ session('error') }}",
            icon: "error",
            showCancelButton: true,
            confirmButtonText: 'Exit',
            cancelButtonText: 'Stay on the page'
        });
    });
</script>
@endif
@endsection