<?php

namespace App\Http\Controllers;

use App\Http\Requests\Client\ClientPaymentStoreRequest;
use App\Http\Requests\Client\EditorClientStoreRequest;
use App\Http\Requests\Client\EditorClientUpdateRequest;
use App\Models\ClientImage;
use App\Models\EditorClient;
use App\Models\ClientPayment;
use App\Models\Country;
use App\Models\HiaringMood;
use App\Models\MedicalStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use PDF;
use Validator;
use Exception;

class EditorClientController extends Controller
{
    public function __construct()
    {
        $this->middleware('checkEditorCompanyClient')->only(['show', 'edit', 'update', 'destroy']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = EditorClient::where('company_id', auth()->user()->company_id)->orderBy('id', 'desc')->paginate(10);
        return view('editor.client.index', compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $allCountries = Country::where('continent_name', 'Asia')->get(['id', 'country_name']);
        $allMedicalStatus = MedicalStatus::all();
        $allHiaringMoods = HiaringMood::all();
        return view('editor.client.create', compact('allCountries', 'allMedicalStatus', 'allHiaringMoods'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EditorClientStoreRequest $request)
    {
        $newClient = new EditorClient;

        $newClient->user_id                 = Auth::user()->id;
        $newClient->company_id              = Auth::user()->company->id;
        $newClient->first_name              = $request->first_name;
        $newClient->last_name               = $request->last_name;
        $newClient->nid                     = $request->nid;
        $newClient->bmet_reg_no             = $request->bmet_reg_no;
        $newClient->date_of_birth           = date('Y-m-d', strtotime(str_replace('/', '-', $request->date_of_birth)));
        $newClient->passport_no             = $request->passport_no;
        $newClient->passport_issue_date     = date('Y-m-d', strtotime(str_replace('/', '-', $request->passport_issue_date)));
        $newClient->passport_expire_date    = date('Y-m-d', strtotime(str_replace('/', '-', $request->passport_expire_date)));
        $newClient->mobile                  = $request->mobile;
        $newClient->mobile_optional         = $request->mobile_optional;
        $newClient->home_mobile             = $request->home_mobile;
        $newClient->country_id              = $request->country_id;
        $newClient->net_amount              = $request->net_amount;
        $newClient->discount_amount         = $request->discount_amount;
        $newClient->paid_amount             = $request->net_amount - $request->discount_amount;
        $newClient->address                 = $request->address;
        $newClient->comments                = $request->comments;
        $newClient->medical_status          = $request->medical_status;
        $newClient->hiaring_mood            = $request->hiaring_mood;
        $newClient->visa_number             = $request->visa_number;
        $newClient->visa_expire_date        = ($request->visa_expire_date) ? date('Y-m-d', strtotime(str_replace('/', '-', $request->visa_expire_date))) : null;

        $saveNewClient = $newClient->save();

        if ($saveNewClient) {
            session()->flash('success', 'Client Created Successfully.');
            return redirect()->route('editor-clients.index');
        } else {
            session()->flash('error', 'Something Happend Wrong');
            return redirect()->route('editor-clients.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(EditorClient $editorClient)
    {
        $countryName = Country::select('country_name')->where('id', $editorClient->country_id)->first();
        $client['country_name'] = $countryName->country_name;
        $clientPayments = ClientPayment::where('client_id', $editorClient->id)->get();
        return view('editor.client.show')->with(['client' => $editorClient, 'clientPayments' => $clientPayments]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(EditorClient $editorClient)
    {
        $allCountries = Country::where('continent_name', 'Asia')->get(['id', 'country_name']);
        $allMedicalStatus = MedicalStatus::all();
        $allHiaringMoods = HiaringMood::all();
        return view('editor.client.create')->with(['client' => $editorClient, 'allCountries' => $allCountries, 'allMedicalStatus' => $allMedicalStatus, 'allHiaringMoods' => $allHiaringMoods]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditorClientUpdateRequest $request, EditorClient $editorClient)
    {
        $updateClient = $editorClient->update([
            'first_name'              => $request->first_name,
            'last_name'               => $request->last_name,
            'nid'                     => $request->nid,
            'bmet_reg_no'             => $request->bmet_reg_no,
            'date_of_birth'           => date('Y-m-d', strtotime(str_replace('/', '-', $request->date_of_birth))),
            'passport_no'             => $request->passport_no,
            'passport_issue_date'     => date('Y-m-d', strtotime(str_replace('/', '-', $request->passport_issue_date))),
            'passport_expire_date'    => date('Y-m-d', strtotime(str_replace('/', '-', $request->passport_expire_date))),
            'mobile'                  => $request->mobile,
            'mobile_optional'         => $request->mobile_optional,
            'home_mobile'             => $request->home_mobile,
            'country_id'              => $request->country_id,
            'net_amount'              => $request->net_amount,
            'discount_amount'         => $request->discount_amount,
            'paid_amount'             => $request->net_amount - $request->discount_amount,
            'address'                 => $request->address,
            'comments'                => $request->comments,
            'medical_status'          => $request->medical_status,
            'hiaring_mood'            => $request->hiaring_mood,
            'visa_number'             => $request->visa_number,
            'visa_expire_date'        => ($request->visa_expire_date) ? date('Y-m-d', strtotime(str_replace('/', '-', $request->visa_expire_date))) : null
        ]);

        if ($updateClient) {
            session()->flash('success', 'Client Updated Successfully.');
            return redirect()->route('editor-clients.index');
        } else {
            session()->flash('error', 'Something Happend Wrong');
            return redirect()->route('editor-clients.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function clientPayment(ClientPaymentStoreRequest $request)
    {
        $client = EditorClient::findOrFail($request->client_id);
        $countryCode = Country::select('country_code')->where('id', $client->country_id)->first();
        $invoiceSlInfo = ClientPayment::select('invoice_sl')->where('client_id', $request->client_id)->orderBy('id', 'DESC')->first();
        $monthYear = Carbon::now();

        $invoiceSl = null;
        if (is_null($invoiceSlInfo)) {
            $invoiceSl = 1;
        } else {
            $invoiceSl = $invoiceSlInfo->invoice_sl + 1;
        }

        $invoiceNo = $monthYear->year . '_' . $monthYear->month . '_' . $countryCode->country_code . '_' . $client->passport_no . '_' . $invoiceSl;

        $newPayment = new ClientPayment;
        $newPayment->client_id      = $request->client_id;
        $newPayment->invoice_no     = $invoiceNo;
        $newPayment->invoice_sl     = $invoiceSl;
        $newPayment->amount         = $request->payment_amount;
        $newPayment->comments       = $request->payment_comments;
        $newPayment->payment_date   = date('Y-m-d', strtotime(str_replace('/', '-', $request->payment_date)));
        $newPayment->user_id        = Auth::user()->id;
        $saveNewPayment = $newPayment->save();

        $updateClient = $client->update([
            'total_amount'  => $client->total_amount + $request->payment_amount
        ]);

        if ($saveNewPayment && $updateClient) {
            $label = null;
            $date = date('d-m-Y', strtotime($newPayment->payment_date));
            $receiverName = Auth::user()->first_name . ' ' . Auth::user()->last_name;
            if (Auth::user()->role->slug == "admin") {
                $label = 'label-success';
            } else if (Auth::user()->role->slug === 'editor') {
                $label = 'label-info';
            } else {
                $label = 'label-warning';
            }
            $data = array(
                'status'            => 201,
                'payment_table_row' => '<tr><td>' . ($request->last_tr_id + 1) . '</td><td>' . $date . '</td><td>' . $newPayment->amount . '</td><td><span class="label ' . $label . '">' . $receiverName . '</span></td><td>' . $newPayment->comments . '</td><td><button type="button" class="btn bg-primary btn-sm"><i class="fa fa-eye"></i></button><button type="button" class="btn bg-info btn-sm"><i class="fa fa-cloud-download"></i></button></td></tr>',
                'total_amount'      => $client->total_amount,
                'tr_id'             => $request->last_tr_id,
                'message'           => 'Payment Save Successfully',
            );
            return response($data);
        } else {
            $data = array(
                'status' => 304,
                'message' => 'Failed. Please Contact to Developer'
            );
            return response($data);
        }
    }

    public function invoicePrint($invId)
    {
        $invoiceInfo = ClientPayment::findOrFail($invId);
        if (!is_null($invoiceInfo)) {
            $clientInfo = EditorClient::findOrFail($invoiceInfo->client_id);
            return view('prints.invoice', compact('invoiceInfo', 'clientInfo'));
        }
    }

    public function invoicePdf($invId)
    {
        $invoiceInfo = ClientPayment::findOrFail($invId);
        if (!is_null($invoiceInfo)) {
            $clientInfo = EditorClient::findOrFail($invoiceInfo->client_id);
            $pdf = PDF::loadView('prints.invoice-pdf', [
                'clientName' => $clientInfo->first_name . ' ' . $clientInfo->last_name,
                'clientAddress' => $clientInfo->address,
                'clientMobile' => $clientInfo->mobile,
                'invoiceNo' => $invoiceInfo->invoice_no,
                'paymentDate' => date('d-m-Y', strtotime($invoiceInfo->payment_date)),
                'paymentAmount' => $invoiceInfo->amount,
                'paymentComment' => $invoiceInfo->comments,
                'paymentComment' => $invoiceInfo->comments,
            ]);
            $pdf->setPaper('A4');
            // return view('prints.invoice-pdf');
            return $pdf->stream('invoice.pdf');
        }
    }

    public function documentUpload(Request $request)
    {
        $clientInfo = EditorClient::find($request->client_id);
        if (!is_null($clientInfo)) {
            if (!$request->hasFile('picture') && !$request->hasFile('nid') && !$request->hasFile('passport')) {
                session()->flash('error', 'Please Select any Image');
                return redirect()->back();
            } else {
                if ($request->hasFile('picture')) {
                    $validator = Validator::make(
                        $request->all(),
                        [
                            'picture' => 'mimes:jpeg,jpg,png|max:1024|dimensions:max_width=600,max_height=720'
                        ],
                        [
                            'picture.mimes'         => 'Client Image must be jpeg, jpg or png.',
                            'picture.max'           => 'Client Image size must be less then 1MB.',
                            'picture.dimensions'    => 'Client Image width must be less than 600px & height must be less then 700px.',
                        ]
                    );
                    $validator->validate();

                    $file = $request->file('picture');
                    $fileBlob = base64_encode(file_get_contents($request->file('picture')->path()));
                    $imageNameSlug = Str::slug($file->getClientOriginalName());
                    $imageName = 'picture_' . $imageNameSlug . '.' . $file->getClientOriginalExtension();
                    $file->move(storage_path('/app/public/' . $clientInfo->passport_no) . '/', 'picture_' . $imageNameSlug . '.' . $file->getClientOriginalExtension());

                    ClientImage::create([
                        'client_id' => $request->client_id,
                        'document_name' => 'picture',
                        'document_path' => $clientInfo->passport_no,
                        'file_name' => $imageName,
                        'file_blob' => $fileBlob
                    ]);
                }

                if ($request->hasFile('nid')) {
                    $validator = Validator::make(
                        $request->all(),
                        [
                            'nid' => 'mimes:jpeg,jpg,png|max:2048|dimensions:max_width=2600,max_height=3600'
                        ],
                        [
                            'nid.mimes'         => 'NID Image must be jpeg, jpg or png.',
                            'nid.max'           => 'NID Image size must be less then 2MB.',
                            'nid.dimensions'    => 'NID Image width must be less than 2600px & height must be less then 3600px.',
                        ]
                    );
                    $validator->validate();

                    $file = $request->file('nid');
                    $fileBlob = base64_encode(file_get_contents($request->file('nid')->path()));
                    $imageNameSlug = Str::slug($file->getClientOriginalName());
                    $imageName = 'nid_' . $imageNameSlug . '.' . $file->getClientOriginalExtension();
                    $file->move(storage_path('/app/public/' . $clientInfo->passport_no) . '/', 'nid_' . $imageNameSlug . '.' . $file->getClientOriginalExtension());

                    ClientImage::create([
                        'client_id' => $request->client_id,
                        'document_name' => 'nid',
                        'document_path' => $clientInfo->passport_no,
                        'file_name' => $imageName,
                        'file_blob' => $fileBlob
                    ]);
                }

                if ($request->hasFile('passport')) {
                    $validator = Validator::make(
                        $request->all(),
                        [
                            'passport' => 'mimes:jpeg,jpg,png|max:2048|dimensions:max_width=2600,max_height=3600'
                        ],
                        [
                            'passport.mimes'         => 'Password Image must be jpeg, jpg or png.',
                            'passport.max'           => 'Password Image size must be less then 2MB.',
                            'passport.dimensions'    => 'Password Image width must be less than 2600px & height must be less then 3600px.',
                        ]
                    );
                    $validator->validate();

                    $file = $request->file('passport');
                    $fileBlob = base64_encode(file_get_contents($request->file('passport')->path()));
                    $imageNameSlug = Str::slug($file->getClientOriginalName());
                    $imageName = 'passport_' . $imageNameSlug . '.' . $file->getClientOriginalExtension();
                    $file->move(storage_path('/app/public/' . $clientInfo->passport_no) . '/', 'passport_' . $imageNameSlug . '.' . $file->getClientOriginalExtension());

                    ClientImage::create([
                        'client_id' => $request->client_id,
                        'document_name' => 'passport',
                        'document_path' => $clientInfo->passport_no,
                        'file_name' => $imageName,
                        'file_blob' => $fileBlob
                    ]);
                }

                session()->flash('success', 'Image Updated Successfully.');
                return redirect()->back();
            }
        } else {
            session()->flash('error', 'Something Happend Wrong');
            return redirect()->back();
        }
    }

    public function editorClientsSearch(Request $request)
    {
        try {
            $query =  EditorClient::where('company_id', auth()->user()->company_id);

            if ($request->filled('name')) {
                $query->where(DB::raw("concat(first_name, ' ', last_name)"), 'LIKE', "%{$request->name}%");
            }

            if ($request->filled('mobile')) {
                $query->where('mobile', 'LIKE', "%{$request->mobile}%");
            }

            if ($request->filled('passport')) {
                $query->where('passport_no', 'LIKE', "%{$request->passport}%");
            }

            $datas = $query->orderBy('id', 'desc')->paginate(10);

            if ($request->ajax()) {
                return view('editor.client.client_render', compact('datas'))->render();
            }
        } catch (Exception $e) {
            dd($e);
            //throw $th;
        }
    }
}
