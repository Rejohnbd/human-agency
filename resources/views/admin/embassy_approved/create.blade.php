@extends('layouts.layout')

@if(isset($embassyApprove))
@section('title', 'Client Update')
@else
@section('title', 'Client Create')
@endif

@section('content')

@component('partials.breadcrumb',[
'title' => isset($embassyApprove) ? 'Client Update' : 'Client Create',
'activePage' => isset($embassyApprove) ? 'Client Update' : 'Client Create'
])
@endcomponent

<section class="content">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">@if(isset($embassyApprove)) Update Client @else Create Client @endif</h3>
        </div>
        <form action="{{ isset($embassyApprove) ? route('embassy-approve.update', $embassyApprove->id) : route('embassy-approve.store') }}" method="POST">
            @csrf
            @if(isset($embassyApprove))
            @method('PUT')
            @endif
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group @error('passport_no') has-error @enderror">
                            <label for="passportNo" class="control-label">Passport Number <span class="text-danger">*</span></label>
                            <input type="text" name="passport_no" class="form-control" id="passportNo" value="{{ isset($embassyApprove) ? $embassyApprove->passport_no : old('passport_no') }}" placeholder="Enter Passport No" required>
                            @error('passport_no')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group @error('name') has-error @enderror">
                            <label for="name" class="control-label">Client Name <span class="text-danger">*</span></label>
                            <input type="text" name="name" class="form-control" id="name" value="{{ isset($embassyApprove) ? $embassyApprove->name : old('name') }}" placeholder="Client Name" required>
                            @error('name')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group @error('country_id') has-error @enderror">
                            <label for="selectCountry" class="control-label">Select Contry <span class="text-danger">*</span></label>
                            <select class="form-control select2" name="country_id" id="selectCountry" style="width: 100%;" required>
                                @if(!isset($embassyApprove))
                                <option>Selecte Country</option>
                                @endif
                                @foreach($allCountries as $country)
                                <option value="{{ $country->id }}" @if(isset($embassyApprove)) @if($country->id == $embassyApprove->country_id) selected @endif @endif>{{ $country->country_name }}</option>
                                @endforeach
                            </select>
                            @error('country_id')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group @error('mobile') has-error @enderror">
                            <label for="phoneNumber" class="control-label">Phone Number </label>
                            <input type="number" name="mobile" class="form-control" id="phoneNumber" value="{{ isset($embassyApprove) ? $embassyApprove->mobile : old('mobile') }}" placeholder="Phone Number">
                            @error('mobile')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group @error('passport_expire_date') has-error @enderror">
                            <label for="passportExpireDate" class="control-label">Passport Expire Date </label>
                            <input type="text" name="passport_expire_date" class="form-control datepicker" id="passportExpireDate" value="{{ isset($embassyApprove->passport_expire_date) ? date('d/m/Y', strtotime($embassyApprove->passport_expire_date)) : old('passport_expire_date') }}" placeholder="Passport Expire Date">
                            @error('passport_expire_date')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group @error('registration_date') has-error @enderror">
                            <label for="registrationDate" class="control-label">Registration Date <span class="text-danger">*</span></label>
                            <input type="text" name="registration_date" class="form-control datepicker" id="registrationDate" value="{{ isset($embassyApprove) ? date('d/m/Y', strtotime($embassyApprove->registration_date)) : old('registration_date') }}" placeholder="Registration Date" required>
                            @error('registration_date')
                            <span class="help-block text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <a href="{{ route('embassy-approve.index') }}" class="btn btn-default">Cancel</a>
                <button type="submit" class="btn btn-info pull-right">@if(isset($embassyApprove)) Update Client @else Create Client @endif</button>
            </div>
        </form>
    </div>
</section>
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/select2/dist/css/select2.min.css') }}">
@endsection

@section('scripts')
<script src="{{ asset('vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
{{-- <script src="{{ asset('plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('plugins/input-mask/jquery.inputmask.extensions.js') }}"></script> --}}
<script src="{{ asset('plugins/select2/dist/js/select2.full.min.js') }}"></script>
<script>
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        todayHighlight: true,
        autoclose: true
    });
    // $('[data-mask]').inputmask();
    // $('.select2').select2();
    // $('.select2').find(':selected').data('Italy');

    $('.select2').select2({
        // ...
        templateSelection: function(data, container) {
            // Add custom attributes to the <option> tag for the selected option
            $(data.element).attr('data-custom-attribute', data.customValue);
            return data.text;
        }
    });
    $('.select2').find(':selected').data('Italy');
</script>
@endsection