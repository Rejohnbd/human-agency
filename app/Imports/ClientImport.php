<?php

namespace App\Imports;

use App\Models\Client;
use App\Models\EmbassyApprove;
use App\Models\HiaringMood;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class ClientImport implements ToModel, WithStartRow
{
    public function startRow(): int
    {
        return 4;
    }

    // public function collection()
    // {
    //     return Client::all();
    // }

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new Client([
            'user_id'               => Auth::user()->id,
            'company_id'            => $row[0],
            'first_name'            => $row[1],
            'last_name'             => $row[2],
            'nid'                   => $row[3],
            'passport_no'           => $row[4],
            'bmet_reg_no'           => $row[5],
            'hiaring_mood'          => $row[6],
            'visa_number'           => $row[7],
            'visa_expire_date'      => $row[8],
            'mobile'                => $row[9],
            'mobile_optional'       => $row[10],
            'home_mobile'           => $row[11],
            'medical_status'        => $row[12],
            'date_of_birth'         => $row[13],
            'passport_issue_date'   => $row[14],
            'passport_expire_date'  => $row[15],
            'country_id'            => $row[16],
            'net_amount'            => $row[17],
            'discount_amount'       => $row[18],
            'paid_amount'           => $row[17] - $row[18],
            'address'               => $row[19],
            'comments'              => $row[20]
        ]);
    }
}
