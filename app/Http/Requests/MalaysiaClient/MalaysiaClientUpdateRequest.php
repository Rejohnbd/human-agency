<?php

namespace App\Http\Requests\MalaysiaClient;

use Illuminate\Foundation\Http\FormRequest;

class MalaysiaClientUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'            => 'required|string',
            'last_name'             => 'required|string',
            'date_of_birth'         => 'required|date_format:d/m/Y',
            'passport_no'           => 'required|string|unique:malaysia_clients,passport_no,' . $this->malaysia_client->id,
            'passport_issue_date'   => 'required|date_format:d/m/Y',
            'passport_expire_date'  => 'required|date_format:d/m/Y',
            'mobile'                => 'required|digits:6|unique:malaysia_clients,mobile,' . $this->malaysia_client->id,
            'home_mobile'           => 'nullable|digits:6',
            'country_id'            => 'required|numeric',
            'net_amount'            => 'required|numeric',
            'medical_status'        => 'required|numeric',
            'discount_amount'       => 'nullable|numeric|lt:net_amount',
            'address'               => 'required|string',
            'comments'              => 'nullable|string',
        ];
    }

    public function messages()
    {
        return [
            'first_name.required'               => 'First Name is Required.',
            'first_name.string'                 => 'Provide Valid First Name.',
            'last_name.required'                => 'Last Name is Required.',
            'last_name.required'                => 'Provide Valid Last Name.',
            'date_of_birth.required'            => 'Date of Birth Required.',
            'date_of_birth.date'                => 'Provide Valid Date of Birth.',
            'passport_no.required'              => 'Passport No is Required.',
            'passport_no.string'                => 'Provide Valid Passport No.',
            'passport_no.unique'                => 'This Passport No Allready Exist.',
            'passport_issue_date.required'      => 'Passport Issue Date is Required.',
            'passport_issue_date.date'          => 'Provide Valid Passport Issue Date Format.',
            'passport_issue_date.date_format'   => 'Provide Valid Passport Issue Date Format.',
            'passport_expire_date.required'     => 'Passport Expire Date is Required.',
            'passport_expire_date.date'         => 'Provide Valid Passport Expire Date Format.',
            'passport_expire_date.date_format'  => 'Provide Valid Passport Expire Date Format.',
            'mobile.required'                   => 'Phone Number is Required.',
            'mobile.digits'                     => 'Provide Valid Phone Number.',
            'mobile.unique'                     => 'Phone Number is Allready Exist.',
            'home_mobile.digits'                => 'Provide Valid Home Phone Number.',
            'country_id.required'               => 'Country Name is Required.',
            'country_id.numeric'                => 'Provide Valid Country Name.',
            'net_amount.required'               => 'Charge Amount is Required.',
            'net_amount.numeric'                => 'Provide Valid Charge Amount.',
            'medical_status.required'           => 'Medical Status Required',
            'medical_status.numeric'            => 'Provide Valid Medical Status',
            'discount_amount.numeric'           => 'Provide Valid Discount Amount.',
            'discount_amount.lt'                => 'Discount Amount Must be Lessthan Charge Amount.',
            'address.required'                  => 'Address is Required.',
            'address.string'                    => 'Provide Valid Address.',
        ];
    }
}
